package org.carl.doc.ee;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.carl.doc.ee.handler.WaterMarkHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.Random;

public class CreateSimpleExcel {
    public static void main(String[] args) throws IOException {

        Random random = new Random();
        int total = random.nextInt(400) + 300;
        int size = 70;
        int page = total / size;
        int pages = (total % size == 0 ? page : page + 1);
        WaterMarkHandler markHandler = new WaterMarkHandler();
        markHandler.setWaterMark("carllongj");
        File file = new File("generateByApplication/doc/watermark.xlsx");
        ExcelWriter writer = EasyExcel.write(new FileOutputStream(file)).inMemory(true).registerWriteHandler(markHandler).build();
        for (int i = 0; i < pages; i++) {
            WriteSheet sheet = EasyExcel.writerSheet("carl" + i).build();
            writer.write(Collections.emptyList(), sheet);
        }
        writer.finish();

        XSSFWorkbook xssfWorkbook = new XSSFWorkbook("generateByApplication/doc/watermark.xlsx");
        SXSSFWorkbook sxssfWorkbook = new SXSSFWorkbook(xssfWorkbook, 100);

        for (int i = 0; i < total; i++) {
            int rowIndex = i % size;
            int value = i / size;
            SXSSFSheet sxssfSheet = sxssfWorkbook.getSheet("carl" + value);
            if (Objects.isNull(sxssfSheet)) {
                sxssfSheet = sxssfWorkbook.createSheet("carl" + value);
            }
            SXSSFRow row = sxssfSheet.getRow(rowIndex);
            if (Objects.isNull(row)) {
                row = sxssfSheet.createRow(rowIndex);
            }

            for (int j = 0; j < 10; j++) {
                SXSSFCell cell = row.getCell(j);
                if (Objects.isNull(cell)) {
                    cell = row.createCell(j);
                }
                cell.setCellValue(String.format("第 %d 行,第 %d 列", i + 1, j + 1));
            }
        }
        sxssfWorkbook.write(new FileOutputStream("generateByApplication/doc/watermarkWithData.xlsx"));
        sxssfWorkbook.close();
    }
}
