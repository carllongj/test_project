package org.carl.doc.ee.image;

import org.carl.doc.ee.handler.WaterMarkHandler;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;

import static org.assertj.core.api.Assertions.assertThat;

public class DrawImageTest {

    @Test
    public void testCreateImage() throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(new File("../generateByApplication/pic/b.png"));
        WaterMarkHandler handler = new WaterMarkHandler();
        handler.drawImage("carllongj", fileOutputStream);
    }

    @Test
    public void testDrawWater() throws Exception {
        File file = new File("../generateByApplication/pic/c.png");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        WaterMarkHandler handler = new WaterMarkHandler();
        handler.drawImage("carllongj", fileOutputStream);
        assertThat(file).isFile().exists().canRead();
    }
}
