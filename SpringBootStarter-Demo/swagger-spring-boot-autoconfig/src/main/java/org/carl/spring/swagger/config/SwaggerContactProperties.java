package org.carl.spring.swagger.config;

import java.util.Objects;

/**
 * 用以配置swagger联系人相关信息
 */
public class SwaggerContactProperties {

    private String name;

    private String url;

    private String email;

    public String getName() {
        return Objects.isNull(this.name) ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return Objects.isNull(this.email) ? "" : email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return Objects.isNull(this.url) ? "" : url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
