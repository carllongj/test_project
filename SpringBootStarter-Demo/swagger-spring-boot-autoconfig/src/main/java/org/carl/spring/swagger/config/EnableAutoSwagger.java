package org.carl.spring.swagger.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 用以手动引入 Swagger 的配置项
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(SwaggerAutoConfig.class)
public @interface EnableAutoSwagger {

}
