package org.carl.spring.swagger.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Objects;

/**
 * 用以配置swagger 相关信息
 */
@ConfigurationProperties("swagger.config")
public class SwaggerConfigProperties {

    private String title;

    private String basePackage;

    private String description;

    private SwaggerContactProperties contact;

    public String getTitle() {
        return Objects.isNull(this.title) ? "" : title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBasePackage() {
        return Objects.isNull(this.basePackage) ? "" : basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }

    public String getDescription() {
        return Objects.isNull(this.description) ? "" : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SwaggerContactProperties getContact() {
        return Objects.isNull(this.contact) ? new SwaggerContactProperties() : contact;
    }

    public void setContact(SwaggerContactProperties contact) {
        this.contact = contact;
    }
}
