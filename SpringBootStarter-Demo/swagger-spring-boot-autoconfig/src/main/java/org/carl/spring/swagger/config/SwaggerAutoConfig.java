package org.carl.spring.swagger.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;

/**
 * 自动装配Swagger信息
 * {@link EnableConfigurationProperties}用于将配置类直接添加到Spring容器中
 */
@Configuration
@EnableSwagger2
@EnableConfigurationProperties(SwaggerConfigProperties.class)
public class SwaggerAutoConfig implements ApplicationListener<EmbeddedServletContainerInitializedEvent> {

    @Bean
    @ConditionalOnClass(value = {Docket.class})
    public Docket createSwaggerConfig(SwaggerConfigProperties swaggerConfigProperties) {
        return new Docket(DocumentationType.SWAGGER_2)
                // 设置api的说明信息
                .apiInfo(apiInfo(swaggerConfigProperties))
                // 添加全局参数
                .globalOperationParameters(Collections.emptyList())
                // 添加选择器
                .select()
                // 选择controller包下的api
                .apis(RequestHandlerSelectors.basePackage(swaggerConfigProperties.getBasePackage()))
                // 所有路径
                .paths(PathSelectors.any())
                .build();
    }

    public ApiInfo apiInfo(SwaggerConfigProperties swaggerConfigProperties) {
        SwaggerContactProperties contact = swaggerConfigProperties.getContact();
        Contact realContact = new Contact(contact.getName(), contact.getUrl(), contact.getEmail());
        return new ApiInfoBuilder().contact(realContact)
                .title(swaggerConfigProperties.getTitle())
                .description(swaggerConfigProperties.getDescription())
                .build();
    }

    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {
        try {
            String address = InetAddress.getLocalHost().getHostAddress();
            String name = event.getApplicationContext().getApplicationName();
            String swagger = String.format("http://%s:%d%s/swagger-ui.html", address, event.getSource().getPort(), name);
            System.out.println("swagger-ui 地址为 : " + swagger);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
