/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.swagger.test.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author carllongj
 * 2022/7/7 22:16
 */
@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping("/hello")
    @ApiOperation("查询当前的默认状态")
    public String test() {
        return "{\"swagger\": \"enable\"}";
    }
}
