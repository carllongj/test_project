/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.swagger.test;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author carllongj
 * 2022/7/7 21:58
 */
@ComponentScan(basePackages = {"org.carl.spring.swagger.test"})
@EnableKnife4j
@EnableAutoConfiguration
public class AutoSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoSwaggerApplication.class, args);
    }
}
