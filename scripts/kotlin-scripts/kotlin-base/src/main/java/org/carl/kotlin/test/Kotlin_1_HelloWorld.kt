package org.carl.kotlin.test

// class 外的函数声明
fun main() {
  println("Hello World")
  val list = mutableListOf(1, 2, 3, 4, 5)
  list.remove(3)
  println(list)

  val testObj = Test("test", 20)
  testObj.printSum(1, 2)
  println(testObj.sum(3, 4))

  val clear = NewClear()
  println(clear.test())
  test("Hello ", " World")

  test()
  val new = NewClear()
  println(new + 1)
}

operator fun NewClear.plus(intValue: Int): NewClear {
  println(intValue)
  return this
}

fun test(a: String, b: String): Unit = println(a + b)

abstract class Clear {
  abstract fun test(): Int
}

class NewClear : Clear() {
  override fun test(): Int {
    return 0
  }
}
