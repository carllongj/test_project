package org.carl.kotlin.test.dsl

fun main() {
  val watcher = AddWatcher()
  watcher.add(custom({
    this.after({

    })
    this.before({ i, s ->
      {

      }
    })
    this.on({
      println(it)
    })
  }))
  watcher.add(custom {
    after {
      println(it)
    }
    before { i, s ->
      println(i)
    }
  })

  watcher.add(newCustom {
    it.after {
      println(it)
    }
    it.before { i, s ->
      println(i)
    }
  })
}

class AddWatcher {
  fun add(watcher: Watcher) {
    watcher.after("123")
  }
}

private inline fun makeTest2(test: String) {
  println(test)
}
