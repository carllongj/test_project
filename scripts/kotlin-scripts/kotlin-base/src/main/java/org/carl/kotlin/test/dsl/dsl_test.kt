package org.carl.kotlin.test.dsl

interface Watcher {
  fun after(s: String): Unit
  fun before(i: Int, s: String): Unit
  fun on(s: String): Unit
}

private typealias after = (s: String) -> Unit
private typealias before = (i: Int, s: String) -> Unit
private typealias on = (s: String) -> Unit

class CustomWatcher : Watcher {

  private var after: after? = null
  private var before: before? = null
  private var on: on? = null

  override fun after(s: String) {
    after?.invoke(s) ?: Unit
  }

  override fun before(i: Int, s: String) {
    before?.invoke(i, s) ?: Unit
  }

  override fun on(s: String) {
    on?.invoke(s) ?: Unit
  }

  fun after(after: after) {
    this.after = after
  }

  fun before(before: before) {
    this.before = before
  }

  fun on(on: on) {
    this.on = on
  }
}

fun custom(customWatcher: CustomWatcher.() -> Unit): CustomWatcher {
  return CustomWatcher().also(customWatcher)
}

fun newCustom(customWatcher: (CustomWatcher) -> Unit): CustomWatcher {
  return CustomWatcher().also(customWatcher)
}
