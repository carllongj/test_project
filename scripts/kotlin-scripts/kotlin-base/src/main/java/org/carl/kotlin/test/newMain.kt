package org.carl.kotlin.test

fun main() {
  val str = "1293"
  println("$str.length = ${str.length}")

  val value = """
    ${'$'}_9.99
  """.trimIndent()
  val simple = "${'$'}_9.93"
  println(value)
  println(simple)


  val set = setOf(1, 2, 3, 4, 5, 6)
  for (i in set) {
    print(i)
  }

  val list = listOf(1, 2, 3, 4, 5, 6)
  for (i in list.indices)
    println("item at $i is ${list[i]}")

  for (i in 1..10) {
    print(i)
  }

  println()

  for (i in 6 downTo 0 step 2)
    print(i)

  println()

  val array = arrayOf(1, 2, 3, 4)
  for ((index, value) in array.withIndex())
    println("the element at $index is $value")

  val x = 1
  val returnValue: Int = when (x) {
    1 -> {
      print("test ")
      10
    }
    2 -> {
      print("2test ")
      20
    }
    else -> 0
  }
  println("the returnValue of whe is $returnValue")
}


fun String.sub(start: Int, end: Int): String = this.substring(start, end)

