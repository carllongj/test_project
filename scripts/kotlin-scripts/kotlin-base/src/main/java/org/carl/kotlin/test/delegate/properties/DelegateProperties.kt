package org.carl.kotlin.test.delegate.properties

import kotlin.reflect.KProperty

class DelegateProperties {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
    return "$thisRef, thank for delegate ${property.name} to me"
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
    println("$value has been assigned to ${property.name} in $thisRef")
  }
}

class Example {
  var pro: String by DelegateProperties()
}

class Example1 {
  val test: String by DelegateProperties()
}

fun main() {
  println(Example().pro)
  Example().pro = "NEW"

  println(Example1().test)
}
