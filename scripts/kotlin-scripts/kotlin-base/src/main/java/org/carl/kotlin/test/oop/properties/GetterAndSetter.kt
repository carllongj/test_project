package org.carl.kotlin.test.oop.properties

fun main() {
  val p = Person()
  println(p.name)
  p.name = "clearlove"
  println(p.name)
}

class Person {
  var name: String = "carl"
    /*get() = field + "clear"
    set(value) {
      field = value + "jps"
    }*/
    get() = field
    set(value) {
      field = value
    }

  /**
   * 不同于默认的 getter 和 setter
   */
  var age: Int = 26
    get() {
      return field - 1
    }
    set(value) {
      field = value + 1
    }
}
