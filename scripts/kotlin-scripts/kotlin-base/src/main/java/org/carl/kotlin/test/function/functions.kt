package org.carl.kotlin.test.function

const val value = "abs"

fun test(i: Int = 10, block: B.() -> Unit) {
  println("test $i")
}

fun test0() {
  println("test0")
}

fun main() {
  test1()

  val value = 1 or 3
}

fun test1() = test0()

class B {
  fun print() {

  }
}
