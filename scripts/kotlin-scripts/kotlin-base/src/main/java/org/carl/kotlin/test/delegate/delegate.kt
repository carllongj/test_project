package org.carl.kotlin.test.delegate

val a = "abs"
const val b = "abc"
fun main() {
  val point1: Point = E().dp
  E().test0()
  println(1.intPlus(2))
  val point: Point? = null
  val a = Impl()
  println(B(a).test())
}

interface A {
  fun test(): Int
}

class Impl : A {
  override fun test(): Int {
    return 10
  }
}

fun test1(vararg i: Int) = Unit

class B(a: A) : A by a

open class C(a: A) : A {
  override fun test(): Int {
    return 10
  }

  open fun test1(): Unit {

  }
}

open class D(a: A) : C(a) {

  override fun test1(): Unit {

  }
}

class E(a: A = Impl()) : D(a) {
  override fun test1() {
    super.test1()
  }
}


val E.dp: Point
  get() = Point(this.test())

// 扩展函数
fun E.test0(): Unit {

}

val intPlus: Int.(Int) -> Int = Int::plus

class Point(value: Int) {

}
