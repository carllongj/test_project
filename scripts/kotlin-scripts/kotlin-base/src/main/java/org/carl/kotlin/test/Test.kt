package org.carl.kotlin.test

var a = 10

class EmptyJ

class Test constructor(str: String, age: Int) {
  val name_: String
  val age_: Int

  init {
    this.name_ = str
    this.age_ = age
  }

  constructor(str: String, age: Int, sex: Boolean) : this(str, age) {
    println(sex)
  }

  constructor(str: String, age: Int, sex: Boolean, health: String) : this(str, age, sex) {
    println(health)
  }

  fun printSum(a: Int, b: Int): Unit {
    println(a + b)
  }

  fun sum(a: Int, b: Int): Int {
    return org.carl.kotlin.test.a + b
  }

  fun add(a: Int, b: Int) = a + b
}

data class Person(val name: String = "carl", var age: Int = 28, val sex: Boolean)

fun test(): Unit {
  val p = Person("carl", 27, true)
  p.age = 12
}

fun main() {
  val p = Test("carl", 29)
  println("person name = ${p.name_} and age = ${p.age_}")
  val p1 = Test("jps", 25, true)
  println("person name = ${p1.name_} and age = ${p1.age_}")
}
