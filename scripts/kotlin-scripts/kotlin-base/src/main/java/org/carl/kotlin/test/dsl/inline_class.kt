package org.carl.kotlin.test.dsl

import org.carl.kotlin.test.delegate.a

inline fun test() {
  println("test")

}

fun test1() {
  println("123")
  test2()
  println(a)
}

fun test2() {
  println("test2")
}
