/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.kotlin.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author carllongj
 *     <p>date 2022/8/4 23:18
 */
public class Main {
  public static void main(String[] args) {
    //
    LocalPerson localPerson = new LocalPerson("carl", 24);
    System.out.println(localPerson.component1());
    System.out.println(localPerson.component2());
    System.out.println(localPerson);
    System.out.println(localPerson.copy("zs", 23));

    UserManager.INSTANCE.test();
  }

  public static void fun() {
    List<String> list = new ArrayList<String>();
    List<? extends Object> value = list;
  }

  public static void copy(Collection<Object> to, Collection<String> from) {
    to.add(from);
  }

  public static void fun1() {
    List<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(2);
    List<? extends Number> valueList;
    valueList = list;
    Number number = valueList.get(1);
    System.out.println(number);
    // compileError
    // valueList.add(new Integer(1));
  }

  public static void fun2() {
    List<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(2);
    List<? super Integer> valueList;
    valueList = list;
    Object object = valueList.get(0);
    valueList.add(1);
  }
}
