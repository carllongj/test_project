package org.carl.kotlin.test

fun main() {
  val a = "abc"
  println(a.length)
  println(a!!.length)
  val aLen = a?.length
  println(aLen)

  val b: String? = null
  //b = null // 可以为null
  println(b?.length)
  val c: Int? = b?.length
  println(c is Int?)

  val d = b!!.length

  val carl: A = A("carl")
  println(carl.department?.header?.name)
  println(carl.test)
  val lj: A = A("lj")
  lj.let { lj.department?.header?.name }
  lj.let({ p: A -> { lj.department?.header?.name } })

  mapOf("a" to "b", "c" to d)

  val strings = listOf("Kotlin", null)
  for (str in strings) {
    str?.let { println(str) }
  }

  "123".sub(0, 1)
}

fun emptyTest() {
  0
}

class A(aName: String, d: Department? = null) {
  val name: String = aName
  val department: Department? = d
  val test by lazy {
    "str"
  }

  /*companion object {

  }*/
  companion object Factory {

  }
}

class Department(p: A?) {
  var header: A? = p
  fun test() {
    val x = A.Factory
    val l: (Int, String) -> Int = { x: Int, s: String ->
      println(s)
      x
    }

    val value = this.let(10) { x: Int, s: String ->
      println(s)
      x
    }

  }

  private fun let(a: Int, block: (Int, String) -> Int): Int {
    return block(1, "21")
  }

  private fun lam(block: (Int, String) -> Int): Int {
    return block.invoke(1, "232")
  }

  fun lam1(test: (Int) -> Int): Int {
    return 0
  }
}
