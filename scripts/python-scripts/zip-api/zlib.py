#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author carllongj
@date 2022/8/3 9:39
"""

import os

import zlib


def compress(infile, dst, level=9):
	infile = open(infile, 'rb')
	dst = open(dst, 'wb')
	compress = zlib.compressobj(level)
	data = infile.read(1024)
	while data:
		dst.write(compress.compress(data))
		data = infile.read(1024)
	dst.write(compress.flush())


def decompress(infile, dst=None):
	infile = open(infile, 'rb')
	dstbytes = bytearray()
	decompress = zlib.decompressobj()
	data = infile.read(1024)
	while data:
		dstbytes += decompress.decompress(data)
		data = infile.read(1024)
	if dst:
		dst = open(dst, 'wb')
		dst.write(dstbytes)
	else:
		print(dstbytes)


# dst.write(decompress.flush())


if __name__ == "__main__":

	path = '../../.git/objects'
	filelist = os.listdir(path)

	for file in filelist:
		newPath = path + '/' + file
		realFiles = os.listdir(newPath)
		for zipFile in realFiles:
			finalPath = newPath + '/' + zipFile
			decompress(finalPath)
			break
		break

# decompress('../../component/git/attr/.git/objects/75/92880024a2b9c85169cc31f308c5356396ca0b')
