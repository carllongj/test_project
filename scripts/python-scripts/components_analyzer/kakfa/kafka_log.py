#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author carllongj
@date 2022/7/30 22:22
"""
import re


class B(object):
	def test(self, key):
		return "hello" + key


class Test(B, object):
	def __init__(self, name, score):
		self.name = name
		self.score = score

	def print(self):
		real_name = super().test(self.name)
		print("name %s ,score: %s" % (real_name, self.score))


t = Test("carllongj", 90)
t.print()

print("s" + "s" + "sk" + str(3) + str(3))

if __name__ == '__main__':
	kk = re.compile(r'\d+')
	value = kk.findall('one1two2three3four4')
	print(value)
