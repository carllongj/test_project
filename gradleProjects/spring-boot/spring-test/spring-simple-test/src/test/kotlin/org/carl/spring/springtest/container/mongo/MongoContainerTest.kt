package org.carl.spring.springtest.container.mongo

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClients
import org.assertj.core.api.Assertions.assertThat
import org.bson.BsonDocument
import org.bson.Document
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.pojo.PojoCodecProvider
import org.bson.types.ObjectId
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.testcontainers.containers.MongoDBContainer

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MongoContainerTest {

  private lateinit var mongoClient: MongoDBContainer

  @BeforeAll
  fun createContainer() {
    mongoClient = MongoDBContainer("mongo:4.2")
    mongoClient.start()
  }

  @Test
  fun testMongoSaveRecord() {
    val pojoCodecRegistry = fromRegistries(
      MongoClientSettings.getDefaultCodecRegistry(),
      fromProviders(PojoCodecProvider.builder().automatic(true).build())
    )

    val settings = MongoClientSettings.builder()
      .codecRegistry(pojoCodecRegistry)
      .applyConnectionString(
        ConnectionString(
          "mongodb://${mongoClient.host}:${mongoClient.firstMappedPort}/test"
        )
      ).build()


    val mongoClient = MongoClients.create(settings)

    val db = mongoClient.getDatabase("test")
    val collection = db.getCollection("t")

    val doc = Document()
    doc["id"] = ObjectId()
    doc["name"] = "carl"
    doc["age"] = 26
    collection.insertOne(doc)

    assertThat(collection.countDocuments()).isEqualTo(1)
    val query = BsonDocument()
    val result = collection.find(query)
    
    result.forEach {
      assertThat(it["name"]).isEqualTo("carl")
      assertThat(it["age"]).isEqualTo(26)
    }
  }

  @AfterAll
  fun destroy() {
    mongoClient.stop()
  }
}
