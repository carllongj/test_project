package org.carl.spring.springtest.nullable

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import kotlin.random.Random

class TestNullable {

  private lateinit var o: String

  @Test
  fun testNullCaller() {

    getString().apply {
      println("test")
    }
  }


  @Test
  fun testIfElse() {
    // getString()后这个必须要为?.调用.否则可能存在两块代码都将执行
    // 因为即使为null.apply中的代码已经执行.并且因为其为null.后面的表达式也将执行
    getString()?.apply {
      println("not null")
    } ?: apply {
      // 执行到此,跟函数无关,表示 getString() 返回值一定为null.
      println("null")
    }
  }

  private fun getString(): String? {
    return if (Random(System.currentTimeMillis())
        .nextInt(10).apply { println(this) } % 2 == 0
    ) "" else null
  }

  @Test
  fun testTypeNull() {
    val sum: Int.(other: Int) -> Int = { other -> this.plus(other) }
    println(1.sum(2))
    println(sum(1, 2))
  }

  @Disabled
  @Test
  fun copyFiles() {
    val input = Files.newInputStream(Paths.get("D:\\.GamingRoot"), StandardOpenOption.READ)
    val output = Files.newOutputStream(Paths.get("D:\\.cp"), StandardOpenOption.CREATE, StandardOpenOption.WRITE)
    copy(input, output)
  }

  private fun copy(`in`: InputStream, output: OutputStream) {
    try {
      // byte Array read
      var read: ByteArray = ByteArray(1024)
      var len: Int = 0
      `in`.use { input ->
        output.use {
          while (input.read(read, 0, read.size).also { len = it } != -1) {
            output.write(read, 0, len)
          }
        }
      }
    } catch (t: Throwable) {
      t.printStackTrace()
    }
  }
}
