package org.carl.spring.springtest.container.mongo

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.testcontainers.containers.PostgreSQLContainer
import java.sql.DriverManager

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PostgresqlContainerTest {

  private lateinit var postgresqlClient: PostgreSQLContainer<*>

  @BeforeAll
  fun createContainer() {
    postgresqlClient = PostgreSQLContainer("postgres:15.4")
      .withUsername("carl")
      .withPassword("123")
      .withDatabaseName("test")
    postgresqlClient.start()
  }

  @Test
  fun testPostgresql() {
    val connection = DriverManager.getConnection(
      "jdbc:postgresql://${postgresqlClient.host}:${postgresqlClient.firstMappedPort}/test",
      "carl", "123"
    )


    connection.createStatement()
      .execute(
        """
      create table t (
      id bigint not null ,
      username varchar(32) not null,
      age int not null,
      primary key(id)
      )
    """.trimIndent()
      )

    connection.createStatement().execute("insert into t values(1, 'carl', 23)")

    val rs = connection.createStatement().executeQuery("select * from t")
    while (rs.next()) {
      println("id -> ${rs.getLong(1)},name -> ${rs.getString(2)},age -> ${rs.getInt(3)}")
    }
  }

  @AfterAll
  fun destroy() {
    postgresqlClient.stop()
  }
}
