package org.carl.spring.springtest.container.redis

import io.lettuce.core.RedisClient
import io.lettuce.core.RedisURI
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.testcontainers.containers.GenericContainer
import java.time.Duration
import java.time.temporal.ChronoUnit

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RedisContainerTest {

  private lateinit var redisContainer: GenericContainer<*>

  @BeforeAll
  fun createRedisContainer() {
    redisContainer = GenericContainer("redis:7.0.12")
      .withExposedPorts(6379)
    redisContainer.start()
  }

  @Test
  fun testSaveRedisRecord() {
    val redisURI = RedisURI.builder().withHost(redisContainer.host)
      .withPort(redisContainer.firstMappedPort)
      .withTimeout(Duration.of(10, ChronoUnit.SECONDS))
      .build()

    val client = RedisClient.create(redisURI)
    val command = client.connect().sync()

    command.set("username", "carl")

    assertThat(command.get("username")).isEqualTo("carl")
  }

  @AfterAll
  fun destroy() {
    redisContainer.stop()
  }
}
