package org.carl.spring.springtest

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@Disabled
@SpringBootTest
class SpringTestApplicationTests {

  @Test
  fun contextLoads() {
  }

}
