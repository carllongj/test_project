package org.carl.spring.springtest.container.mysql

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.testcontainers.containers.MySQLContainer
import java.sql.DriverManager

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MysqlContainerTest {

  private lateinit var mysqlClient: MySQLContainer<*>

  @BeforeAll
  fun createContainer() {
    mysqlClient = MySQLContainer("mysql:8.0.30")
      .withUsername("carl")
      .withPassword("123")
      .withDatabaseName("test")
    mysqlClient.start()
  }

  @Test
  fun testMySQLSaveRecord() {
    val connection = DriverManager.getConnection(
      "jdbc:mysql://${mysqlClient.host}:${mysqlClient.firstMappedPort}/test",
      "carl", "123"
    )


    connection.createStatement()
      .execute(
        """
      create table t (
      id bigint not null ,
      username varchar(32) not null,
      age int not null,
      primary key(id)
      )engine = innodb default character set = utf8mb4
    """.trimIndent()
      )

    connection.createStatement().execute("insert into t values(1, 'carl', 23)")

    val rs = connection.createStatement().executeQuery("select * from t")
    while (rs.next()) {
      println("id -> ${rs.getLong(1)},name -> ${rs.getString(2)},age -> ${rs.getInt(3)}")
    }
  }

  @AfterAll
  fun destroy() {
    mysqlClient.stop()
  }
}
