package org.carl.spring.springtest.controller

import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotBlank

@RestController
@RequestMapping("test")
@Validated
class TestController {

  @GetMapping("test")
  fun test(@NotBlank id: String): Map<String, Int> {
    val l = listOf('j', 'q', 'k', 'a')
    for ((i, j) in l.withIndex()) {
      println("${i} -> ${l[i]} -> ${j}")
    }
    return mapOf("age" to 12)
  }
}
