import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.nio.file.Paths

plugins {
  id("java") // 支持Java与Kotlin混编
  id("org.springframework.boot") version "2.7.7"
  id("io.spring.dependency-management") version "1.0.15.RELEASE"
  kotlin("jvm") version "1.7.20"
  kotlin("plugin.spring") version "1.7.20"
}

group = "org.carl.spring"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17
buildDir = Paths.get("target").toFile()

repositories {
  mavenLocal()
  mavenCentral()
}

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("org.springframework.boot:spring-boot-starter-data-redis")
  implementation("jakarta.validation:jakarta.validation-api:2.0.2")
  implementation("org.hibernate:hibernate-validator:6.1.7.Final")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  implementation("org.mongodb:mongodb-driver-sync:4.7.2")
  implementation("com.mysql:mysql-connector-j:8.0.33")
  implementation("org.postgresql:postgresql:42.6.0")


  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.testcontainers:testcontainers:1.18.3")
  testImplementation("org.testcontainers:mongodb:1.18.3")

  // test for mysql postgres
  testImplementation("org.testcontainers:jdbc:1.18.3")
  testImplementation("org.testcontainers:mysql:1.18.3")
  testImplementation("org.testcontainers:postgresql:1.18.3")
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "17"
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}
