package org.carl.spring.webflux.controller

import org.carl.spring.webflux.bean.Person
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class SimpleController {

  @GetMapping("/webflux/ping")
  fun test(): Mono<String> {
    return Mono.just("PONG")
  }

  @GetMapping("/webflux/cost", produces = [MediaType.APPLICATION_JSON_VALUE])
  fun cost(): Mono<Person> {

    val r = Mono.create {
      Thread.sleep(2000)
      it.success(Person("carl", 23))
    }
    println("already execute,prepare to return")
    return r
  }
}
