package org.carl.spring.webflux

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * 配置类
 */
@SpringBootApplication
class Application


/**
 * 启动函数
 */
fun main(args: Array<String>) {
  runApplication<Application>(*args)
}
