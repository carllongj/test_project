package org.carl.spring.webflux.bean

data class Person(val name: String, val age: Int)
