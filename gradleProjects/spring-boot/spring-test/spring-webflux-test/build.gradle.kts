import java.nio.file.Paths

plugins {
  id("java")
  id("org.springframework.boot") version "2.7.7"
  id("io.spring.dependency-management") version "1.0.15.RELEASE"
  kotlin("jvm") version "1.7.20"
  kotlin("plugin.spring") version "1.7.20"
}

group = "org.carl.spring"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17
buildDir = Paths.get("target").toFile()

repositories {
  mavenLocal()
  mavenCentral()

}

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-webflux")
  testImplementation(platform("org.junit:junit-bom:5.9.1"))
  testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
  useJUnitPlatform()
}
