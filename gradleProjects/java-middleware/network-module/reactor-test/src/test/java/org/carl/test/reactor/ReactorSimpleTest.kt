package org.carl.test.reactor

import org.junit.jupiter.api.Test
import org.springframework.util.StopWatch
import reactor.core.publisher.Mono

class ReactorSimpleTest {

  @Test
  fun testJust() {
    val mono = Mono.just(Person("carl", 22))
    println(mono.block())
  }

  @Test
  fun active() {
    val stopWatch = StopWatch()
    stopWatch.start("async")
    val r1 = getResult()
    stopWatch.stop()

    stopWatch.start("sync")
    println(getBlockResult())
    stopWatch.stop()
    stopWatch.start("record")
    println(r1.block())
    stopWatch.stop()
    println(stopWatch.prettyPrint())
  }

  private fun getBlockResult(): String {
    Thread.sleep(2000)
    return "success"
  }

  private fun getResult(): Mono<String> {
    return Mono.create {
      Thread.sleep(2000)
      it.success("success")
    }
  }
}

data class Person(val name: String, val age: Int)
