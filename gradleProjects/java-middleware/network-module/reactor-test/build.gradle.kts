import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.nio.file.Paths

plugins {
  id("java") // 支持Java与Kotlin混编
  kotlin("jvm") version "1.7.20"
}

group = "org.carl.component.reactor"
version = "1.0"
java.sourceCompatibility = JavaVersion.VERSION_17
buildDir = Paths.get("target").toFile()

repositories {
  mavenLocal()
  mavenCentral()
}

dependencies {
  implementation("io.projectreactor:reactor-core:3.4.21")
  implementation("org.springframework:spring-core:6.0.11")

  testImplementation("org.junit.jupiter:junit-jupiter-api:${rootProject.extra["junit_version"]}")
  testImplementation("org.junit.jupiter:junit-jupiter-engine:${rootProject.extra["junit_version"]}")
}


tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "17"
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}


