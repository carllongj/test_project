/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.component.net.ssh.test;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.session.ClientSession;

import java.io.IOException;

/**
 * @author carllongj
 * <p>date 2022/8/10 23:23
 */
public class ApacheSshd {
	public static void main(String[] args) throws IOException, InterruptedException {
		//
		SshClient sshClient = SshClient.setUpDefaultClient();
		sshClient.start();
		ClientSession session =
			sshClient.connect("carllongj", "192.168.222.140", 22).verify().getSession();
		session.addPasswordIdentity("123");
		System.out.println(session.auth().verify().isSuccess());
		String remoteCommand = session.executeRemoteCommand("pwd");
		System.out.println(remoteCommand);

		session.close(false);
		sshClient.close(false);
		while (!sshClient.isClosed()) {
			//
		}
		Thread.sleep(4000);
	}
}
