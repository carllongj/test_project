/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.component.net.ssh.test;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Scanner;

/**
 * @author carllongj
 *     <p>date 2022/8/10 22:48
 */
public class Main {

  public static void main(String[] args) throws JSchException, IOException {
    //

    JSch jSch = new JSch();
    Session session = jSch.getSession("carllongj", "192.168.222.130", 22);
    Properties config = new Properties();
    config.put("StrictHostKeyChecking", "no");
    session.setConfig(config);

    session.setPassword("123");

    session.connect(30000);
    Channel channel = session.openChannel("shell");
    channel.connect(3000);

    // 读取终端返回的信息流
    InputStream inputStream = channel.getInputStream();
    Scanner scanner = new Scanner(System.in);
    try {
      // 循环读取
      byte[] buffer = new byte[1024];
      int i = 0;
      while ((i = inputStream.read(buffer)) != -1) {
        System.out.println(new String(buffer, 0, i));
        new Thread(
                () -> {
                  while (true) {
                    String s = scanner.next();
                    try {
                      channel.getOutputStream().write((s + '\n').getBytes(StandardCharsets.UTF_8));
                      channel.getOutputStream().write("exit".getBytes(StandardCharsets.UTF_8));
                      channel.getOutputStream().flush();

                    } catch (IOException e) {
                      throw new RuntimeException(e);
                    }
                  }
                })
            .start();
      }

    } finally {
      // 断开连接后关闭会话
      session.disconnect();
      channel.disconnect();
      if (inputStream != null) {
        inputStream.close();
      }
    }
  }
}
