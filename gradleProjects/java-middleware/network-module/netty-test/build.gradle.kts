import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.nio.file.Paths

plugins {
  id("java") // 支持Java与Kotlin混编
  kotlin("jvm") version "1.7.20"
}


group = "org.carl.component.network.netty"
version = "1.0-SNAPSHOT"

java.sourceCompatibility = JavaVersion.VERSION_17
buildDir = Paths.get("target").toFile()

repositories {
  mavenLocal()
  mavenCentral()
}


dependencies {
  implementation("io.netty:netty-all:4.1.77.Final")
}


tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "17"
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}


