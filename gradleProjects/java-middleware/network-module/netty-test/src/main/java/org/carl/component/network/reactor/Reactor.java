/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.component.network.reactor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * @author carllongj
 *     <p>date 2022/8/7 16:47
 */
public class Reactor {

  final Selector selector;

  final ServerSocketChannel serverSocketChannel;

  public Reactor() throws IOException {
    SelectorProvider selectorProvider = SelectorProvider.provider();
    this.selector = null;
    this.serverSocketChannel = ServerSocketChannel.open();
    serverSocketChannel.bind(new InetSocketAddress(7890));
    serverSocketChannel.configureBlocking(false);
    SelectionKey key = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
    key.attach(new Acceptor());
  }

  public static void main(String[] args) throws IOException {
    //
    System.out.println(System.getProperty("java.nio.channels.spi.SelectorProvider"));

		ServiceLoader<SelectorProvider> sl =
			ServiceLoader.load(SelectorProvider.class,
				ClassLoader.getSystemClassLoader());

    new Reactor();
  }

  private class Acceptor implements Runnable {

    @Override
    public void run() {
      try {
        while (!Thread.interrupted()) {
          selector.select();
          Set<SelectionKey> keySet = selector.selectedKeys();
          Iterator<SelectionKey> iterator = keySet.iterator();
          while (iterator.hasNext()) {
            SelectionKey selectionKey = iterator.next();
            // dispatch(selectionKey);
          }
          keySet.clear();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
