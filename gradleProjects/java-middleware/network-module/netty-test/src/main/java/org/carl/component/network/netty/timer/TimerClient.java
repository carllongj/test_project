/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.component.network.netty.timer;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.carl.component.network.netty.timer.streaming.SimpleTimerClientHandler;
import org.carl.component.network.netty.timer.streaming.TimeDecoder;

/**
 * @author carllongj
 *     <p>date 2022/8/11 21:18
 */
public class TimerClient {
  public static void main(String[] args) {
    //
    NioEventLoopGroup master = new NioEventLoopGroup(1);
    try {
      Bootstrap bootstrap =
          new Bootstrap()
              .group(master)
              .channel(NioSocketChannel.class)
              .option(ChannelOption.SO_KEEPALIVE, true)
              .handler(
                  new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                      /*
                      // 面向未出现 粘包 情况的处理
                      ch.pipeline().addLast(new TimerClientHandler());
                      */
                      /*
                                 // 面向可能出现 粘包 情况的处理,在添加以及移除handler时,增加一个缓冲区
                                 ch.pipeline().addLast(new SimpleTimerClientHandler());
                      */
                      ch.pipeline().addLast(new TimeDecoder(), new TimerClientHandler());
                    }
                  });

      ChannelFuture future = bootstrap.connect("127.0.0.1", 37).sync();
      future.channel().closeFuture().sync();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      master.shutdownGracefully();
    }
  }
}
