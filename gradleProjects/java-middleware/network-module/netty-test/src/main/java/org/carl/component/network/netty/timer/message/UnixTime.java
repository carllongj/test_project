/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.component.network.netty.timer.message;

import java.util.Date;

/**
 * @author carllongj
 *     <p>date 2022/8/11 21:42
 */
public class UnixTime {
  private final long value;

  public UnixTime() {
    this(System.currentTimeMillis() / 1000L + 2208988800L);
  }

  public UnixTime(long timeMills) {
    this.value = timeMills;
  }

  public long getValue() {
    return value;
  }

  @Override
  public String toString() {
    return new Date((getValue() - 2208988800L) * 1000L).toString();
  }
}
