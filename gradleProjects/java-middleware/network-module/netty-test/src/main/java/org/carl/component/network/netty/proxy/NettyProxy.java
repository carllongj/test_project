/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.component.network.netty.proxy;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.carl.component.network.netty.proxy.handler.ProxyHandler;

/**
 * 最简陋的 Netty 实现 Proxy 的逻辑
 *
 * @author carllongj
 *     <p>date 2022/8/11 15:53
 */
public class NettyProxy {

  private void initServer(NioEventLoopGroup master, NioEventLoopGroup worker) {
    try {
      ServerBootstrap bootstrap =
          new ServerBootstrap()
              .group(master, worker)
              .channel(NioServerSocketChannel.class)
              .handler(new LoggingHandler(LogLevel.DEBUG))
              .childHandler(
                  new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                      ProxyHandler handler = new ProxyHandler();
                      handler.setChannel(initClient(master, ch));
                      ch.pipeline().addLast("serverHandler", handler);
                    }
                  })
              .option(ChannelOption.SO_BACKLOG, 128);
      ChannelFuture future = bootstrap.bind(8989).sync();
      future.channel().closeFuture().sync();
    } catch (Exception e) {
      master.shutdownGracefully();
      worker.shutdownGracefully();
    }
  }

  private Channel initClient(NioEventLoopGroup boss, SocketChannel socketChannel)
      throws InterruptedException {
    Bootstrap bootstrap =
        new Bootstrap()
            .group(boss)
            .channel(NioSocketChannel.class)
            .handler(
                new ChannelInitializer<SocketChannel>() {
                  @Override
                  protected void initChannel(SocketChannel ch) throws Exception {
                    ProxyHandler handler = new ProxyHandler();
                    handler.setChannel(socketChannel);
                    ch.pipeline().addLast("clientHandler", handler);
                  }
                });
    ChannelFuture future = bootstrap.connect("httpbin.org", 80).sync();
    /* // SSH 服务端代理
    ChannelFuture future = bootstrap.connect("192.168.222.130", 22).sync();
    */
    /* // Redis 的服务端代理
     ChannelFuture future = bootstrap.connect("192.168.222.130", 6379).sync();
    */
    return future.channel();
  }

  public static void main(String[] args) {
    NettyProxy proxy = new NettyProxy();
    NioEventLoopGroup master = new NioEventLoopGroup(2);
    NioEventLoopGroup worker = new NioEventLoopGroup();
    proxy.initServer(master, worker);
  }
}
