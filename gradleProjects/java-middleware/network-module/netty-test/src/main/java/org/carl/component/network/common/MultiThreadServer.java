/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.component.network.common;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author carllongj
 *     <p>date 2022/8/7 16:25
 */
public class MultiThreadServer {

  public static void main(String[] args) throws IOException {
    // 可以使用多个线程来调用 accept 方法,提升效率
    ServerSocket serverSocket = new ServerSocket(7890);

    new Thread(new ServerDaemon(serverSocket), "server-1").start();
    new Thread(new ServerDaemon(serverSocket), "server-2").start();

    while (true) {}
  }

  private static class ServerDaemon implements Runnable {

    private final ServerSocket serverSocket;

    private ServerDaemon(ServerSocket serverSocket) {
      this.serverSocket = serverSocket;
    }

    @Override
    public void run() {
      try {
        while (true) {
          Socket accept = serverSocket.accept();
          System.out.println(Thread.currentThread().getName() + " accept connection " + accept);
          Thread.sleep(10);
        }
      } catch (IOException | InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
