/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.kafka.component;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author carllongj
 *     <p>date 2022/7/30 16:28
 */
public class KafkaProducerMain {
  public static void main(String[] args) throws IOException {
    //
    Properties prop = new Properties();
    InputStream inputStream =
        KafkaConsumerMain.class.getResourceAsStream("/kafka-producer.properties");
    prop.load(inputStream);
    // 设置 事务ID
    prop.put("transactional.id", UUID.randomUUID().toString());
    prop.put("acks", "all");
    Producer<String, String> producer = new KafkaProducer<>(prop);
    producer.initTransactions();
    int i = 200000;
    producer.beginTransaction();
    while (i-- > 0) {
      Future<RecordMetadata> future =
          producer.send(
              new ProducerRecord<>("test-topic", "carllongj-localhost:9092/" + i));
    }
    producer.commitTransaction();
  }

  /**
   * 同步发送信息
   *
   * @param producer
   * @throws ExecutionException
   * @throws InterruptedException
   */
  public static void sendSync(Producer<String, String> producer)
      throws ExecutionException, InterruptedException {
    producer
        .send(new ProducerRecord<>("test-topic", "carllongj", "carllongj-localhost:9092/"))
        .get();
  }

  /**
   * 异步发送消息
   *
   * @param producer
   * @return
   */
  public static Future<RecordMetadata> sendAsync(Producer<String, String> producer) {
    return producer.send(
        new ProducerRecord<>("test-topic", "carllongj", "carllongj-localhost:9092/"));
  }
}
