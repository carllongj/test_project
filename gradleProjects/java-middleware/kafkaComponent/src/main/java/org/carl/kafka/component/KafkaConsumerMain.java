/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.kafka.component;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

/**
 * @author carllongj
 *     <p>date 2022/7/30 16:39
 */
public class KafkaConsumerMain {

  private static Properties consumer() throws IOException {
    //
    Properties prop = new Properties();
    InputStream inputStream =
        KafkaConsumerMain.class.getResourceAsStream("/kafka-consumer.properties");
    prop.load(inputStream);
    return prop;
  }

  private static Properties notAutoCommitConsumer() throws IOException {
    Properties properties = consumer();
    properties.put("enable.auto.commit", "false");
    return properties;
  }

  public static void main(String[] args) throws IOException {
    Properties prop = notAutoCommitConsumer();
    Consumer<String, String> consumer = new KafkaConsumer<>(prop);
    consumer.subscribe(Collections.singletonList(prop.getProperty("topic.name")));
    Map<TopicPartition, Long> offsets =
        consumer.beginningOffsets(
            Collections.singletonList(new TopicPartition(prop.getProperty("topic.name"), 0)));
    // consumer.subscribe(Collections.singletonList(prop.getProperty("topic.name")));

    while (true) {
      ConsumerRecords<String, String> records = consumer.poll(Duration.of(1000, ChronoUnit.MILLIS));
      if (!records.isEmpty()) {
        for (ConsumerRecord<String, String> record : records) {
          System.out.printf("offset = %d, value = %s \n", record.offset(), record.value());
        }
        consumer.commitSync();
      }
    }
  }
}
