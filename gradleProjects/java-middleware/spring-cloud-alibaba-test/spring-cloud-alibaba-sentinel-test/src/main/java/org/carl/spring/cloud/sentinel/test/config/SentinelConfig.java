/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.cloud.sentinel.test.config;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author carllongj
 *     <p>date 2022/8/2 10:55
 */
@Component
public class SentinelConfig {

  @PostConstruct
  public void init() {
    initDegradeRule();
  }

  private void initDegradeRule() {
    List<DegradeRule> degradeRuleList = new ArrayList<>();
    DegradeRule degradeRule = new DegradeRule();
    degradeRule.setResource("hello");
    degradeRule.setCount(5);
    degradeRule.setGrade(RuleConstant.DEGRADE_GRADE_EXCEPTION_COUNT);
    degradeRule.setTimeWindow(80);
    degradeRuleList.add(degradeRule);
    DegradeRuleManager.loadRules(degradeRuleList);
  }

  private void initFlowRule() {
    List<FlowRule> flowRules = new ArrayList<>();
    FlowRule flowRule = new FlowRule();
    flowRule.setCount(20);
    flowRule.setGrade(RuleConstant.FLOW_GRADE_QPS);
    flowRule.setLimitApp("default");
    flowRules.add(flowRule);
    FlowRuleManager.loadRules(flowRules);
  }
}
