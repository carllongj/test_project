/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.cloud.gateway.test;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author carllongj
 *     <p>date 2022/8/2 18:40
 */
@Component
// 1. 添加 @Component 注解
public class TempGatewayFilterFactory
    extends AbstractGatewayFilterFactory<TempGatewayFilterFactory.Config> {

  // 添加 无参构造方法,将当前的Config类传递给父类
  public TempGatewayFilterFactory() {
    super(Config.class);
  }

	// 重写该方法,将属性进行绑定
  @Override
  public List<String> shortcutFieldOrder() {
    return Arrays.asList("enable");
  }

  @Override
  public GatewayFilter apply(Config config) {
    System.out.println(config.getEnable());
    return (exchange, chain) -> chain.filter(exchange);
  }

  public static class Config {
    private int enable;

    public int getEnable() {
      return enable;
    }

    public void setEnable(int enable) {
      this.enable = enable;
    }
  }
}
