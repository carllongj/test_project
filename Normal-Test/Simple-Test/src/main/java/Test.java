/*
 * Copyright 2019 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;

/** @author carllongj 2019/5/7 18:52 */
public class Test {
  public static void main(String[] args) throws Exception {
    /*Method method = Test.class.getDeclaredMethod("test", String.class);
    Parameter[] parameters = method.getParameters();
    for (Parameter parameter : parameters) {
        System.out.println(parameter.isNamePresent() ? parameter.getName() : "No Name");
    }*/

    int ip1 = 0xa2bc1c9;
    System.out.println(Integer.toHexString(ip1 & 0xfffff000));

    int ip2 = 0xa2bcfdd;
    System.out.println(Integer.toHexString(ip2 & 0xfffff000));

    double value = 3.1455926;
    System.out.printf("%.2f%n", value);

    for (int i = 0; i < 20; i++) {
      Random random = new Random();
      double v = random.nextDouble() * 100;
      System.out.printf("origin value: %s, round value: %d%n", v, Math.round(v));
    }
  }

  public static void test(String jdk) {
    try {
      throw new StackOverflowError();
    } catch (Error error) {
      System.out.println("eee");
    }
  }
}
