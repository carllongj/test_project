
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author carllongj
 * @date 2019/2/12 23:03
 */
public class TestResource {

    public static void main(String[] args) throws Exception {
        InputStream stream = ClassLoader.getSystemResourceAsStream("TESTFILE");
        System.out.println(toString(stream));

        System.out.println(System.getProperty("user.dir"));

        stream = new FileInputStream("src/TESTFILE");
        System.out.println(toString(stream));
    }

    public static String toString(InputStream inputStream) throws Exception {
        byte[] data = new byte[32];
        int len;
        StringBuilder sb = new StringBuilder();
        while ((len = inputStream.read(data)) != -1) {
            sb.append(new String(data, 0, len));
        }
        return sb.toString();
    }
}
