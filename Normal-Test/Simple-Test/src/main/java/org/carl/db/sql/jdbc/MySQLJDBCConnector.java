package org.carl.db.sql.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLJDBCConnector {
  public static void main(String[] args) throws SQLException {
    //
    Connection connection =
        DriverManager.getConnection(
            "jdbc:mysql://192.168.206.135:3306/test_database?useSSL=false", "root", "123456");

    PreparedStatement preparedStatement = connection.prepareStatement("select * from j");
    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      System.out.println(resultSet.getString(1));
    }
  }
}
