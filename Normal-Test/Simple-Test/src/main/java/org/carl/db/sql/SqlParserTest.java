package org.carl.db.sql;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Distinct;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

import java.io.StringReader;

public class SqlParserTest {
  public static void main(String[] args) throws JSQLParserException {
    //
    CCJSqlParserManager manager = new CCJSqlParserManager();
    Statement statement = manager.parse(new StringReader("select distinctfrom test"));
    if (statement instanceof Select) {
      PlainSelect selectBody = (PlainSelect) ((Select) statement).getSelectBody();
      Distinct distinct = selectBody.getDistinct();
      System.out.println(distinct);
    }
  }
}
