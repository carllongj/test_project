package org.carl.date;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * @author longjie
 * 2021/4/13
 */
public class LocalDateApiTest {

    public static final DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("+8"));

    public static void main(String[] args) {
        LocalDateTime dateTime = LocalDateTime.parse("2021-01-01 12:00:00", timestampFormatter);
        LocalDateTime start = LocalDateTime.of(dateTime.toLocalDate(), LocalTime.of(0, 0, 0));
        System.out.println(Timestamp.valueOf(start));
    }
}
