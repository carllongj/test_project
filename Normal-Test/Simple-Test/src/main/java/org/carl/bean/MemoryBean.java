/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.carl.bean;

/**
 * @author longjie
 * 2020/12/6
 */
public class MemoryBean {

    /**
     * 1KB_字节
     */
    private static final int ONE_KB = 1 << 10;

    /**
     * 100 KB
     */
    private static final int HUNDRED_KB = 100 * ONE_KB;

    /**
     * 500KB
     */
    private static final int FIVE_HUNDRED_KB = 5 * HUNDRED_KB;

    /**
     * 1MB
     */
    private static final int ONE_MB = FIVE_HUNDRED_KB * 2;


    /**
     * 2MB
     */
    private static final int TWO_MB = 2 * ONE_MB;

    /**
     * 4MB内存
     */
    private static final int FOUR_MB = 2 * TWO_MB;


    /**
     * 占据内存空间
     */
    private final byte[] data;

    public MemoryBean(byte[] data) {
        this.data = data;
    }

    /**
     * 指定空间大小
     *
     * @param size 指定的内存空间大小
     * @return 返回实例
     */
    public static MemoryBean getBean(int size) {
        return new MemoryBean(new byte[size]);
    }

    public static MemoryBean oneKb() {
        return new MemoryBean(new byte[ONE_KB]);
    }

    public static MemoryBean hundredKb() {
        return new MemoryBean(new byte[HUNDRED_KB]);
    }

    public static MemoryBean fiveHundredKb() {
        return new MemoryBean(new byte[FIVE_HUNDRED_KB]);
    }

    public static MemoryBean oneMb() {
        return new MemoryBean(new byte[ONE_MB]);
    }

    public static MemoryBean twoMb() {
        return new MemoryBean(new byte[TWO_MB]);
    }

    public static MemoryBean fourMb() {
        return new MemoryBean(new byte[FOUR_MB]);
    }
}
