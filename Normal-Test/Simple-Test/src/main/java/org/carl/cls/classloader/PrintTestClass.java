package org.carl.cls.classloader;

/**
 * @author longjie
 * 2021/4/2
 */
public class PrintTestClass {

    public static void fun() {
        StackTraceElement[] stackTrace = new RuntimeException().getStackTrace();
        for (int i = 0; i < stackTrace.length; i++) {
            System.out.println(stackTrace[i].toString());
        }
    }
}
