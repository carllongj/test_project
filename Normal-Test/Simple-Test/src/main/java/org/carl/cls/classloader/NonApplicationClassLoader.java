package org.carl.cls.classloader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author longjie
 * 2021/4/2
 */
public class NonApplicationClassLoader extends ClassLoader {

    private ClassLoader loader = null;

    public void setLoader(ClassLoader loader) {
        this.loader = loader;
    }


    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return loadClass(name, false);
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        synchronized (getClassLoadingLock(name)) {
            Class<?> c = findLoadedClass(name);
            System.out.println(String.format("当前类加载器 %s 扫描Class %s,该类是否已被加载 %s", this, name, c));
            if (c == null) {
                long t0 = System.nanoTime();
                try {
                    c = loader.loadClass(name);
                } catch (ClassNotFoundException e) {
                    c = findClass(name);
                }

                if (c == null) {
                    // If still not found, then invoke findClass in order
                    // to find the class.
                    long t1 = System.nanoTime();
                    c = findClass(name);

                    // this is the defining class loader; record the stats
                    sun.misc.PerfCounter.getParentDelegationTime().addTime(t1 - t0);
                    sun.misc.PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
                    sun.misc.PerfCounter.getFindClasses().increment();
                }
            }

            if (resolve) {
                myResolveClass(c);
            }
            return c;
        }


    }

    public void myResolveClass(Class<?> clazz) {
        resolveClass(clazz);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] bytes = getClassBytes(name);
        return defineClass(name, bytes, 0, bytes.length);
    }


    private byte[] getClassBytes(String name) {
        try {

            if ("B".equals(name)) {

                return Files.readAllBytes(Paths.get("extClass/B.class"));
            } else {
                String path = name.replace('.', '/');
                return Files.readAllBytes(Paths.get("target/classes/" + path + ".class"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException();
    }

    public static void main(String[] args) throws Exception {
        ClassLoader extLoader = ClassLoader.getSystemClassLoader().getParent();
        System.out.println(extLoader.getClass().getName());
        NonApplicationClassLoader classLoader = new NonApplicationClassLoader();
        NonApplicationClassLoader otherClassLoader = new NonApplicationClassLoader();
        classLoader.setLoader(extLoader);
        otherClassLoader.setLoader(extLoader);
//        Class.forName("java.lang.Object", false, classLoader);
//        Class.forName("java.lang.Exception", false, classLoader);
//        Class<?> bClass = Class.forName("B", true, classLoader);
        Class<?> bClass = classLoader.loadClass("B", false);
        classLoader.loadClass("java.lang.Object", false);
        classLoader.loadClass("java.lang.Object", false);
        otherClassLoader.loadClass("java.lang.Object", false);
        System.out.println(String.format("Loader of class B is %s", bClass.getClassLoader()));
        bClass.getDeclaredMethod("fun", (Class<?>[]) null).invoke(null, (Object[]) null);
    }
}
