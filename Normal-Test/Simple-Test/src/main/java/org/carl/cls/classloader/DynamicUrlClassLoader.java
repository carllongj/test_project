/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.cls.classloader;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author carllongj date 2022/7/14 0:07
 */
public class DynamicUrlClassLoader {
  public static void main(String[] args) throws Exception {
    String antHome = System.getenv("ANT_HOME");
    System.out.println(antHome);
    if (Objects.nonNull(antHome) && antHome.length() > 0) {
      File file = new File(antHome, "lib");
      if (file.length() == 0) {
        return;
      }
      List<URL> list = new ArrayList<>();
      for (File jarFile : file.listFiles()) {
        list.add(jarFile.toURI().toURL());
      }
      ClassLoader loader = DynamicUrlClassLoader.class.getClassLoader();
      System.out.println(loader);
      URLClassLoader urlClassLoader = new URLClassLoader(list.toArray(new URL[0]));

      if (args.length > 0) {
        String className = args[0];
        Class<?> aClass = urlClassLoader.loadClass(className);
        System.out.println(aClass);
      }
    }
  }
}
