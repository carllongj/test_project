package org.carl.keyword;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class EnumTest {

  public class A {
    private A() {
      throw new NullPointerException();
    }

    public void fun() {
      System.out.println("fun");
    }
  }

  public static void main(String[] args) throws Exception {
    getA().fun();
  }

  private static A getA() throws Exception {
    Field o = Unsafe.class.getDeclaredField("theUnsafe");
    o.setAccessible(true);
    Unsafe unsafe = (Unsafe) o.get(null);
    return (A) unsafe.allocateInstance(A.class);
  }
}
