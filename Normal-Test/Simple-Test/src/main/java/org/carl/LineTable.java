/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl;


import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author carllongj
 * 2021/1/23 23:02
 */
public class LineTable {
    public static void main(String[] args) throws Exception {
        List<String> list = Files.readAllLines(Paths.get("C:\\Users\\carllongj\\Desktop\\a.txt"), StandardCharsets.UTF_8);
        for (int i = 0; i < list.size(); i++) {
            if ((i % 3) == 2) {
                System.out.println(list.get(i));
            } else {
                System.out.print(list.get(i) + "\t");
            }
        }
    }

    public DocumentFilter fun(Integer value){
        return (String doc) -> {
            System.out.println(doc);
            return true;
        };
    }

    @FunctionalInterface
    interface DocumentFilter {
        boolean match(String s);
    }
}
