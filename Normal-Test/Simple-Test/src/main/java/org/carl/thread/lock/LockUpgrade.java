package org.carl.thread.lock;

import org.carl.bean.Bean;
import org.openjdk.jol.info.ClassLayout;

/**
 * 锁升级 (由hashCode()方法导致的锁膨胀)
 *
 * @author longjie
 * 2021/1/29
 */
public class LockUpgrade {

    public static void main(String[] args) throws InterruptedException {
        /*
            需要sleep一段时间，因为java对于偏向锁的启动是在启动几秒之后才激活。
            因为jvm启动的过程中会有大量的同步块，且这些同步块都有竞争，如果一启动就启动
            偏向锁，会出现很多没有必要的锁撤销
         */
        Thread.sleep(5000);
        Bean bean = new Bean();
        //此时 bean 并未被获取为锁,打印其对象头信息
        System.out.println(ClassLayout.parseInstance(bean).toPrintable());
        synchronized (bean) {
            // 第一次获取锁
            System.out.println(ClassLayout.parseInstance(bean).toPrintable());
        }

        System.out.println(System.identityHashCode(bean));
        System.out.println(ClassLayout.parseInstance(bean).toPrintable());

        synchronized (bean) {
            System.out.println(ClassLayout.parseInstance(bean).toPrintable());
        }
    }
}
