package org.carl.thread.async;

import java.util.concurrent.CompletableFuture;

public class DifferenceInAsync {

  public static void main(String[] args) throws InterruptedException {
    //
    CompletableFuture<Integer> future = new CompletableFuture();
    new Thread(
            () -> {
              try {
                System.out.println(Thread.currentThread().getName() + " -> " + "线程启动");
                Thread.sleep(5000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              System.out.println(Thread.currentThread().getName() + " -> " + "future 完成");
              future.complete(100);
              System.out.println(Thread.currentThread().getName() + " -> " + "执行完成");
            })
        .start();

    // 此处若加此代码,则 whenComplete 方法则由 main线程执行.
    // 若是去掉该代码,则 whenComplete 方法则由 子线程执行.
    Thread.sleep(10000L);

    // 逻辑是,whenComplete 的执行取决于哪个线程执行 complete 方法.
    // 当线程执行了 f.complete时,whenComplete 还未被执行到(事件还未注册),该线程就不会去执行
    // whenComplete的回调,此时哪个线程执行到 whenComplete时,就由哪个线程来执行 whenComplete.

    future.whenComplete(
        (i, ex) -> {
          System.out.println(Thread.currentThread().getName() + " -> " + "do something");
          try {
            Thread.sleep(10000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          System.out.println(Thread.currentThread().getName() + " -> " + "complete end");
        });
  }
}
