/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.carl.thread;

/**
 * @author longjie
 * 2020/12/11
 */
public class ThreadState {

    private static class Test {

        public synchronized void lock() {
            while (true) {
            }
        }
    }

    private static class Run implements Runnable {

        private Test test;

        public Run(Test test) {
            this.test = test;
        }

        @Override
        public void run() {
            test.lock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Test test = new Test();

        Runnable target = new Run(test);
        Thread t1 = new Thread(target, "name-1");
        Thread t2 = new Thread(target, "name-2");
        Thread t3 = new Thread(target, "name-3");

        t1.start();
        t2.start();
        t3.start();

        Thread.sleep(2000);
        System.out.println(String.format("%s -> %s, %s -> %s ,%s -> %s"
                , t1.getName(), t1.getState(), t2.getName(), t2.getState(), t3.getName(), t3.getState()));

        t2.interrupt();


        Thread.sleep(2000);
        System.out.println(String.format("%s -> %s, %s -> %s ,%s -> %s"
                , t1.getName(), t1.getState(), t2.getName(), t2.getState(), t3.getName(), t3.getState()));

    }
}
