package org.carl.thread.async;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class WaitContinueMultiThread {
  public static void main(String[] args) {
    //    postMultiThread();
    //    postCompletableFuture();
    supplyAsync();
  }

  private static void postMultiThread() {
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    List<Integer> integers = Arrays.asList(1, 2, 3);
    CountDownLatch latch = new CountDownLatch(integers.size());
    for (Integer key : integers) {
      executorService.submit(
          () -> {
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            System.out.println("result: " + key);
            latch.countDown();
          });
    }

    executorService.shutdown();
    try {
      latch.await();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private static void postCompletableFuture() {
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    List<Integer> integers = Arrays.asList(1, 2, 3);
    CompletableFuture.allOf(
            integers.stream()
                .map(
                    key ->
                        CompletableFuture.runAsync(
                            () -> {
                              try {
                                Thread.sleep(1000);
                              } catch (InterruptedException e) {
                                e.printStackTrace();
                              }
                              System.out.println("result: " + key);
                            },
                            executorService))
                .toArray(CompletableFuture[]::new))
        .join();
    executorService.shutdown();
  }

  private static void supplyAsync() {
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    List<Integer> integers = Arrays.asList(1, 2, 3);
    CompletableFuture[] futures =
        integers.stream()
            .map(
                key ->
                    CompletableFuture.supplyAsync(
                        () -> {
                          try {
                            Thread.sleep(1000);
                          } catch (InterruptedException e) {
                            e.printStackTrace();
                          }
                          return "test" + key;
                        },
                        executorService))
            .toArray(CompletableFuture[]::new);
    CompletableFuture<Void> future = CompletableFuture.allOf(futures);
    CompletableFuture<List<Object>> completableFuture =
        future.thenApplyAsync(
            v -> Arrays.stream(futures).map(CompletableFuture::join).collect(Collectors.toList()));
    System.out.println(completableFuture.join());
    executorService.shutdown();
  }
}
