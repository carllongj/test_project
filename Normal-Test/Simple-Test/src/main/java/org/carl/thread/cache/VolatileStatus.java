package org.carl.thread.cache;

import java.util.Random;

/**
 * @author longjie
 * 2021/1/27
 */
public class VolatileStatus {

    /**
     * 此处的 volatile 将决定b是否能够被读取到其值
     */
    private volatile static boolean flag = false;

    private static int b = 0;

    public static void main(String[] args) {
        long i = Integer.MAX_VALUE;

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!flag) {
                    //if (b == 20) break;
                }
                int k = Integer.MAX_VALUE;
                while (k-- > 0) {

                }
                while (true) {
                    if (b == 20) break;
                }
                System.out.println("end");
            }
        }).start();
        while (i-- > 0) {

        }
        System.out.println("modify");
        i = Integer.MAX_VALUE;
        while (i-- > 0) {

        }
        b = 20;
        i = 20;
        while (i-- > 0) {
            long k = Integer.MAX_VALUE;
            while (k-- > 0) {

            }
        }
        flag = getResult();
        int j = 0;
        /*
         * 此段代码的验证是为了看在 volatile 变量更新后在多执行的一个时间段让另一个线程不能及时捕获到 字段b 的更新,按照
         * 理论来说,此时更新 b字段 并不能保证 b字段 能够刷新到主存中,因这个 b字段 不是volatile变量.
         *
         * 但实际的效果是在读取 flag 变量后,另一个线程已经能捕捉到当前的 b字段 的更新.
         */
        i = Integer.MAX_VALUE;
        while (i-- > 0) {

        }
        while (true) {
            if (j == 0) {
                System.out.println("con");
                j++;
            }
        }
    }

    private static boolean getResult() {
        Random random = new Random();
        return 0 == random.nextInt(1);
    }
}
