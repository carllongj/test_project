/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.thread.cas;

import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @author carllongj
 * 2022/6/23 21:15
 */
public class ABATest {

    public static void main(String[] args) {
        AtomicStampedReference<Integer> reference = new AtomicStampedReference<>(0, 0);
        Integer integer = reference.getReference();
        int stamp = reference.getStamp();
        System.out.println(reference.compareAndSet(0, 4, 0, 1));
    }
}
