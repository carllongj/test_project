/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.carl.thread;

/**
 * @author longjie
 * 2021/1/14
 */
public class ThreadInterruptTest {

    static class Lock {
        public synchronized void handle() throws InterruptedException {
            this.wait(10000);
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Lock lock = new Lock();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lock.handle();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        Thread.sleep(2000);
        thread.interrupt();
        System.out.println("finish");
    }
}
