/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.carl.thread;

/**
 * @author longjie
 * 2020/12/11
 */
public class ThreadInterrupt {


    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            System.out.println("--------即将开始休眠--------");
            while (true) {
                boolean interrupted = Thread.currentThread().isInterrupted();
                if (interrupted) {
                    break;
                }
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            System.out.println("-------- 线程结束---------");
        });

        t1.start();

        System.out.println("------Main线程运行中------");
        t1.interrupt();
        System.out.println("-------Main结束------");
    }
}
