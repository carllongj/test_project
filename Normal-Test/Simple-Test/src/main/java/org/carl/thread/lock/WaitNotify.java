package org.carl.thread.lock;

/**
 * @author longjie
 * 2021/2/2
 */
public class WaitNotify {
    static class Pool {
        int value = 0;

        private final int max;

        public Pool(int max) {
            this.max = max;
        }

        final Object lock = new Object();

        public void inc() throws InterruptedException {
            synchronized (lock) {
                while (value >= max) {
                    lock.notifyAll();
                    lock.wait();
                }
                value++;
                System.out.println("当前创造的元素为 : " + value);
            }
        }

        public void dec() throws InterruptedException {
            synchronized (lock) {
                //当前无剩余,唤醒生产者,并且等待
                while (value <= 0) {
                    lock.notifyAll();
                    lock.wait();
                }
                value--;
                System.out.println("当前剩余的元素为 : " + value);
            }
        }
    }

    public static void main(String[] args) {
        Pool pool = new Pool(10);
        Thread producer = new Thread(() -> {
            try {
                while (true) {
                    pool.inc();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "生产者1");
        producer.start();

        Thread consumer = new Thread(() -> {
            try {
                while (true) {
                    pool.dec();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "消费者1");
        consumer.start();

        Thread consumer2 = new Thread(() -> {
            try {
                while (true) {
                    pool.dec();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "消费者2");

        consumer2.start();
    }
}
