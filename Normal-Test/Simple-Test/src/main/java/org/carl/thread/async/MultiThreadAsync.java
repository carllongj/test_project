package org.carl.thread.async;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class MultiThreadAsync {

  public static void main(String[] args) {
    //
    // postExecutorService();
    // postCompletableFuture();
    postCompletableJoin();
  }

  /** 通过线程池实现多任务并发 */
  private static void postExecutorService() {
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    List<Integer> list = Arrays.asList(1, 2, 3);
    List<Future<String>> futures = new ArrayList<>();
    for (Integer key : list)
      futures.add(
          executorService.submit(
              () -> {
                try {
                  // 模拟处理过程
                  Thread.sleep(1000);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                return "result : " + key;
              }));

    for (Future<String> future : futures) {
      try {
        System.out.println(future.get());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    executorService.shutdown();
  }

  private static void postCompletableFuture() {
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    List<Integer> list = Arrays.asList(1, 2, 3);
    for (Integer integer : list) {
      CompletableFuture.supplyAsync(
              () -> {
                try {
                  Thread.sleep(1000);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                return "result: " + integer;
              },
              executorService)
          .whenCompleteAsync(
              (result, exception) -> {
                System.out.println(result);
              });
    }
    executorService.shutdown();
  }

  private static void postCompletableJoin() {
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    List<Integer> list = Arrays.asList(1, 2, 3);
    List<String> strings =
        list.stream()
            .map(
                key ->
                    CompletableFuture.supplyAsync(
                        () -> {
                          try {
                            Thread.sleep(1000);
                          } catch (InterruptedException e) {
                            e.printStackTrace();
                          }
                          return "result: " + key;
                        },
                        executorService))
            .map(CompletableFuture::join)
            .collect(Collectors.toList());

    System.out.println(strings);
    executorService.shutdown();
  }
}
