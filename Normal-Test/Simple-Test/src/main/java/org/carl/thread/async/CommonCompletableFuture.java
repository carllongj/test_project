package org.carl.thread.async;

import java.util.Objects;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CommonCompletableFuture {

  public static void main(String[] args) {
    //
    ExecutorService executorService = Executors.newFixedThreadPool(2);
    CompletableFuture<String> completableFuture =
        CompletableFuture.supplyAsync(
                () -> {
                  System.out.println(Thread.currentThread().getName() + " -> " + "1.开始淘米");
                  System.out.println(Thread.currentThread().getName() + " -> " + "2.淘米完成");
                  return "洗净的米";
                },
                executorService)
            .thenApply(
                s -> {
                  System.out.println(Thread.currentThread().getName() + " -> " + "获取到: " + s);
                  System.out.println(Thread.currentThread().getName() + " -> " + "3.开始煮饭");
                  if (new Random().nextInt(10) > 5) {
                    throw new RuntimeException("4. 电饭煲坏了,煮不了");
                  }
                  return "4.煮饭完成";
                })
            .handle(
                (s, e) -> {
                  if (Objects.nonNull(e)) {
                    System.out.println(Thread.currentThread().getName() + " -> " + e.getMessage());
                    return "没饭吃";
                  }
                  System.out.println(Thread.currentThread().getName() + " -> " + s);
                  return "恰饭";
                });

    System.out.println(Thread.currentThread().getName() + " -> " + completableFuture.join());
    executorService.shutdown();
  }
}
