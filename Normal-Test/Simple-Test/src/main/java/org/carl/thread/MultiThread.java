package org.carl.thread;

/**
 * @author longjie
 * 2021/2/5
 */
public class MultiThread {

    public static void main(String[] args) {
        new Thread(() -> {
            long value = Integer.MAX_VALUE;
            while (value-- > 0) ;
            throw new NullPointerException();
        }).start();

        while (true) ;
    }
}
