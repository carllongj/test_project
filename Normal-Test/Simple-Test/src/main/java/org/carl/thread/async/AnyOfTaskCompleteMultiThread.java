package org.carl.thread.async;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** 单个任务执行完成即结束执行 */
public class AnyOfTaskCompleteMultiThread {

  public static void main(String[] args) {
    postCompletableFuture();
  }

  private static void postCompletableFuture() {
    ExecutorService executorService = Executors.newFixedThreadPool(3);

    List<Integer> list = Arrays.asList(1, 2, 3);

    CompletableFuture<Object> completableFuture =
        CompletableFuture.anyOf(
            list.stream()
                .map(
                    key ->
                        CompletableFuture.supplyAsync(
                            () -> {
                              try {
                                Thread.sleep(1000);
                              } catch (InterruptedException e) {
                                e.printStackTrace();
                              }
                              return "result: " + key;
                            },
                            executorService))
                .toArray(CompletableFuture[]::new));
    executorService.shutdown();

    System.out.println(completableFuture.join());
  }
}
