/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.thread.cache;

/**
 * @author carllongj
 * 2021/2/27 12:10
 */
public class OrderVolatile {

    private static Obj obj = null;

    private static class Obj {
        public Obj() {
            while (true) ;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new Thread(OrderVolatile::singleton, "create-thread").start();

        Thread.sleep(1000);
        new Thread(() -> {
            System.out.println("---------start------------");
            System.out.println(singleton());
            System.out.println("---------end------------");
        }, "get-thread").start();
    }

    static int count = 0;
    public static Obj singleton() {
        if (count == 0){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (null == obj) {
            synchronized (OrderVolatile.class) {
                if (null == obj) {
                    obj = new Obj();
                }
                while (true) ;
            }
        }
        return obj;
    }
}
