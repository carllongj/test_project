package org.carl.thread.async;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncCompletableFuture {

  public static void main(String[] args) {
    //
    postCompletableFuture();
  }

  /** 通过 CompletableFuture */
  private static void postCompletableFuture() {

    ExecutorService executorService = Executors.newFixedThreadPool(2);

    String result =
        CompletableFuture.supplyAsync(
                () -> {
                  try {
                    Thread.sleep(1000);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }

                  return "result: 1";
                },
                executorService)
            .thenApplyAsync(
                result1 -> {
                  try {
                    Thread.sleep(1000);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                  return result1 + " \t result: 2";
                },
                executorService)
            .join();

    executorService.shutdown();
    System.out.println(result);
  }
}
