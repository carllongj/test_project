package org.carl.thread.state;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author longjie
 * 2021/1/27
 */
public class ThreadState {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        reader.readLine();
        createBusyThread();
        Object lock = new Object();
        createLockThread(lock);
    }

    private static void createLockThread(final Object lock) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "lock_thread").start();

    }

    private static void createBusyThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) ;
            }
        }, "busy_thread").start();
    }
}
