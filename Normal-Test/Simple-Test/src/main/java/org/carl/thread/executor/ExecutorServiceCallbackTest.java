/*
 * Copyright 2021 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.thread.executor;

import java.util.*;
import java.util.concurrent.*;

/**
 * @author carllongj
 * 2021/6/9 21:37
 */
public class ExecutorServiceCallbackTest {
    /**
     * 线程池
     */
    private static ExecutorService executorService = new ThreadPoolExecutor(3, 3, 3000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(80));

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<Future<Integer>> futureList = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            Future<Integer> future = executorService.submit(new Job());
            futureList.add(future);
        }
        if (!Objects.equals(60, futureList.size())) {
            throw new RuntimeException("不相等的任务");
        }
        long total = 0;
        while (!futureList.isEmpty()) {
            Iterator<Future<Integer>> iterator = futureList.iterator();
            while (iterator.hasNext()) {
                Future<Integer> future = iterator.next();
                if (future.isDone()) {
                    total += future.get();
                    iterator.remove();
                }
            }
        }
        System.out.println("total = " + total);
        executorService.shutdownNow();
    }

    private static class Job implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            Thread.sleep(new Random().nextInt(1000));
            return 1;
        }
    }
}
