/*
 * Copyright 2021 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.quartz;

import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;

import java.text.ParseException;

/**
 * @author carllongj
 * 2021/8/1 10:24
 */
public class QuartzJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("test");
    }

    public static void main(String[] args) throws SchedulerException, ParseException {
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        JobKey jobKey = JobKey.jobKey("TEST_JOB");
        JobDetailImpl jobDetail = new JobDetailImpl();

        jobDetail.setJobClass(QuartzJob.class);
        jobDetail.setKey(jobKey);
        scheduler.scheduleJob(jobDetail, TriggerBuilder.newTrigger().forJob(jobKey).withSchedule(
                CronScheduleBuilder.cronSchedule(new CronExpression("0/5 * * * * ? *"))).build());
        scheduler.start();
    }
}
