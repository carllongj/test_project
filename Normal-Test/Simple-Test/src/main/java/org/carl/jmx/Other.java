/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.carl.jmx;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

/**
 * @author longjie
 * 2020/12/7
 */
public class Other extends NotificationBroadcasterSupport implements OtherMXBean {

    private String name;


    @Override
    public MBeanNotificationInfo[] getNotificationInfo() {
        String[] types = new String[]{AttributeChangeNotification.ATTRIBUTE_CHANGE};

        String name = AttributeChangeNotification.class.getName();
        String description = "简单描述";
        MBeanNotificationInfo info = new MBeanNotificationInfo(types, name, description);
        return new MBeanNotificationInfo[]{info};
    }

    @Override
    public void print(String str) {
        System.out.println(str);
    }

    @Override
    public String getName() {
        return name;
    }

    private int val = 0;

    @Override
    public synchronized void setName(String name) {
        String oldName = this.name;
        this.name = name;
        Notification notification = new AttributeChangeNotification(this, val++, System.currentTimeMillis(),
                "name changed", "name", "int", oldName, name);
        sendNotification(notification);
    }
}
