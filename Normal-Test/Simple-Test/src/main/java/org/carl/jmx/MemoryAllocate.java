/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.carl.jmx;

import org.carl.bean.MemoryBean;

import java.util.HashMap;
import java.util.Map;

/**
 * @author longjie
 * 2020/12/6
 */
public class MemoryAllocate implements MemoryAllocateMBean {

    private Map<String, MemoryBean> map = new HashMap<>();

    private String jsp;

    @Override
    public void oneKb(String id) {
        map.put(id, MemoryBean.oneKb());
    }

    @Override
    public void hundredKb(String id) {
        map.put(id, MemoryBean.hundredKb());
    }

    @Override
    public void fiveHundredKb(String id) {
        map.put(id, MemoryBean.fiveHundredKb());
    }

    @Override
    public void oneMb(String id) {
        map.put(id, MemoryBean.oneMb());
    }

    @Override
    public void twoMb(String id) {
        map.put(id, MemoryBean.twoMb());
    }

    @Override
    public void fourMb(String id) {
        map.put(id, MemoryBean.fourMb());
    }

    @Override
    public void removeBean(String id) {
        map.remove(id);
    }

    @Override
    public void setName(String name) {
        this.jsp = name;
    }

    @Override
    public String getName() {
        return this.jsp;
    }

    @Override
    public Map<String, MemoryBean> getMap() {
        return map;
    }
}
