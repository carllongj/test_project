package org.carl.ansi.literal;

public class StringLiteral {

  public static void main(String[] args) {
    //
    String str = "软件";
    String str1 = new StringBuilder("软").append("件").toString();
    String str2 = "软件";
    System.out.println(str1.intern() == str2);
  }
}
