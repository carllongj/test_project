package org.carl.memory;

import java.lang.management.ManagementFactory;
import java.util.Random;

public class CheatEngine {

  private static final Random RANDOM = new Random();

  public static void main(String[] args) {
    loadCurrentPid();
    unknownInitializeValueScan();
  }

  private static void loadCurrentPid() {
    String name = ManagementFactory.getRuntimeMXBean().getName();
    Integer pid = Integer.parseInt(name.split("@", -1)[0]);
    System.out.printf("%x\n", pid);
  }

  private static void unknownInitializeValueScan() {
    int anInt = RANDOM.nextInt(300000) + 100000;
    int temp = anInt;
    int value = 0;
    while (true) {
      try {
        Thread.sleep(5000);
        System.out.println("value prepare decrease by one,times: " + ++value);
        anInt--;
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (temp == anInt) {
        System.out.printf("Yes,correct value is %d\n", temp);
        break;
      }
    }
  }

  private static void extraScan() {
    while (true) {
      int anInt = RANDOM.nextInt(400000);
      System.out.println(anInt);
      for (int i = 0; i < 5; i++) {
        try {
          Thread.sleep(4000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("current value = " + anInt);
      }
    }
  }
}
