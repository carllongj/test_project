package org.carl.channel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * 效率对比
 * 在虚拟机中 单核 512M内存的linux centOS7系统中
 * 通过内存拷贝的时间少于 通过 零拷贝 的方式,即CPU copy(14330)的时间小于 DMA copy(16355)
 * CPU 使用的占用时长 CPU copy(14个TIME),DMA copy(7个TIME)
 *
 * @author longjie
 * 2021/1/26
 */
public class FileChannelTest {

    static String input = "E:\\meworkspace\\first.rar";

    static String output = "E:\\meworkspace\\back";

    public static void main(String[] args) throws Exception {
        if (null != args && args.length == 2) {
            input = args[0];
            output = args[1];
        }
        //System.out.println(copyCostTime());
        System.out.println(transferCostTime());
    }

    /**
     * @return
     * @throws Exception
     */
    private static int transferCostTime() throws Exception {
        List<Long> list = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            long start = System.currentTimeMillis();
            FileChannel channel = FileChannel.open(Paths.get(input), StandardOpenOption.READ);
            FileChannel copy = FileChannel.open(Paths.get(output),
                    StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
            channel.transferTo(0, channel.size(), copy);
            list.add(System.currentTimeMillis() - start);
            Path path = Paths.get(output);
            path.toFile().delete();
        }

        int value = 0;
        for (Long val : list) {
            value += val.intValue();
        }
        return value / 7;
    }

    private static int copyCostTime() throws Exception {
        List<Long> list = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            long start = System.currentTimeMillis();
            FileInputStream inputStream = new FileInputStream(input);
            FileOutputStream fos = new FileOutputStream(output);
            byte[] arr = new byte[1024];
            int len;
            while (-1 != (len = inputStream.read(arr))) {
                fos.write(arr, 0, len);
            }
            fos.flush();
            fos.close();
            list.add(System.currentTimeMillis() - start);
            Path path = Paths.get(output);
            path.toFile().delete();
        }

        int value = 0;
        for (Long val : list) {
            value += val.intValue();
        }
        return value / 7;
    }
}
