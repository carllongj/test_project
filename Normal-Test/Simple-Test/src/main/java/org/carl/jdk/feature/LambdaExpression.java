/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.jdk.feature;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author carllongj
 *     <p>date 2022/8/4 22:38
 */
public class LambdaExpression {

  static Map<String, Supplier<Object>> map = new HashMap<>();

  /** Map 中的对象不会只是一个 */
  static {
    map.put("carl", () -> new Object());
    map.put(
        "inner_carl",
        new Supplier<Object>() {
          @Override
          public Object get() {
            return new Object();
          }
        });
  }

  public static void main(String[] args) {
    map.put("carl_m", () -> new Object());
    //
    System.out.println(map.get("carl").get() == map.get("carl").get());
    System.out.println(map.get("carl_m").get() == map.get("carl_m").get());
    System.out.println(map.get("inner_carl").get() == map.get("inner_carl").get());

    test();
    test();
  }

  static void test() {
    Runnable runnable = () -> System.out.println("test");
    System.out.println(runnable);
  }
}
