package org.carl.sys.net.spider;

import java.util.Map;
import java.util.function.Consumer;

/**
 * @author longjie
 * 2021/4/29
 */
public class RequestHandler {

    private String method;

    private String url;

    private Map<String, String> headers;

    private Consumer<String> consumer;

    public RequestHandler() {
    }

    public RequestHandler(String url, Consumer<String> consumer) {
        this.url = url;
        this.consumer = consumer;
    }

    public RequestHandler(String url, Map<String, String> headers, Consumer<String> consumer) {
        this.url = url;
        this.headers = headers;
        this.consumer = consumer;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Consumer<String> getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer<String> consumer) {
        this.consumer = consumer;
    }
}
