/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.carl.sys.net;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author longjie
 * 2021/1/26
 */
public class TcpBuffer {
    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(4399);
        //设置接收的缓冲区大小
        //serverSocket.setReceiveBufferSize(20 * 1024 * 1024);
        Socket socket = serverSocket.accept();
        while (true) {
            Thread.sleep(2000);
            System.out.println(String.format("r : %s,s : %s", socket.getReceiveBufferSize(), socket.getSendBufferSize()));
        }
    }
}