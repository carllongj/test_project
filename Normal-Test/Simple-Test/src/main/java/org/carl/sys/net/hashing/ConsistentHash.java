package org.carl.sys.net.hashing;

import java.util.Collection;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author longjie
 * 2021/4/9
 */
public class ConsistentHash<T> {
    private final Hash hash;

    private final int replicas;

    private final SortedMap<Long, T> circle = new TreeMap<>();

    public ConsistentHash(Hash hash, int replicas, Collection<T> nodes) {
        this.hash = hash;
        this.replicas = replicas;
        for (T node : nodes) {
            add(node);
        }
    }

    public void add(T node) {
        for (int i = 0; i < this.replicas; i++) {
            circle.put(this.hash.hash(node.toString() + i), node);
        }
    }

    public void remove(T node) {
        for (int i = 0; i < this.replicas; i++) {
            circle.remove(this.hash.hash(node.toString() + i));
        }
    }

    public T get(String name) {
        if (circle.isEmpty()) {
            return null;
        }

        long hash = this.hash.hash(name);
        if (!circle.containsKey(hash)) {
            SortedMap<Long, T> tailMap = circle.tailMap(hash);
            hash = tailMap.isEmpty() ? circle.firstKey() : tailMap.firstKey();
        }
        return circle.get(hash);
    }
}
