package org.carl.sys.net.hashing;

import org.carl.sys.net.hashing.node.MachineNode;

import java.util.*;

/**
 * @author longjie
 * 2021/4/9
 */
public class HashMock {

    private static final String IP_PREFIX = "192.168.0.";

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        List<MachineNode> nodes = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            map.put(IP_PREFIX + i, 0);
            MachineNode node = new MachineNode(IP_PREFIX + i, "node_" + i);
            nodes.add(node);
        }

        Hash hash = new HashImpl();

        ConsistentHash<MachineNode> consistentHash = new ConsistentHash<>(hash, 5, nodes);
        for (int i = 0; i < 10000; i++) {
            String data = UUID.randomUUID().toString() + i;
            MachineNode machineNode = consistentHash.get(data);
            map.put(machineNode.getIp(), map.get(machineNode.getIp()) + 1);
        }

        for (int i = 1; i <= 10; i++) {
            System.out.println(IP_PREFIX + i + "节点记录条数：" + map.get(IP_PREFIX + i));
        }
    }
}
