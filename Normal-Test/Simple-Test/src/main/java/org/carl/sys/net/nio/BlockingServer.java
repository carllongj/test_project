/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.sys.net.nio;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * 等同于 Socket API,为阻塞式
 *
 * @author longjie
 * 2021/1/25
 */
public class BlockingServer {

    private static class Run implements Runnable {

        private SocketChannel socketChannel;

        Run(SocketChannel socketChannel) {
            this.socketChannel = socketChannel;
        }

        @Override
        public void run() {
            try {
                ByteBuffer buffer = ByteBuffer.allocate(128);
                byte[] arr = new byte[128];
                int len = 0;
                while (-1 != (len = socketChannel.read(buffer))) {
                    buffer.flip();
                    buffer.get(arr, 0, len);
                    String info = new String(arr, 0, len);
                    System.out.println(info);
                    buffer.clear();
                    if ("bye\n".equalsIgnoreCase(info)) {
                        System.out.println("关闭服务端");
                        buffer.put("close\n".getBytes());
                        buffer.flip();
                        socketChannel.write(buffer);
                        socketChannel.shutdownOutput();
                        break;
                    } else if (info.startsWith("file:")) {
                        String substring = info.substring(5, info.length() - 1);
                        System.out.println("read a file,path is : " + substring);
                        byte[] allBytes = Files.readAllBytes(Paths.get(substring));
                        ByteBuffer byteBuffer = ByteBuffer.allocate(allBytes.length);
                        byteBuffer.put(allBytes);
                        buffer.flip();
                        socketChannel.write(byteBuffer);
                    } else {
                        buffer.put((info + "\n").getBytes());
                        buffer.flip();
                        socketChannel.write(buffer);
                    }
                    buffer.clear();
                }
                socketChannel.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        ServerSocketChannel socketChannel = (ServerSocketChannel)
                ServerSocketChannel.open().bind(new InetSocketAddress(4399))
                        .configureBlocking(true);
        while (true) {
            SocketChannel channel = socketChannel.accept();
            new Thread(new Run(channel)).start();
        }
    }
}
