/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.sys.net;

import java.io.ByteArrayOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author longjie
 * 2021/1/21
 */
public class Client {

    public static void main(String[] args) throws Exception {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress("172.16.30.57", 4399));
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            socket.getOutputStream().write((line + "\n").getBytes());
            if (line.equals("bye")) {
                socket.shutdownOutput();
                String read = read(socket);
                System.out.println(read);
                socket.close();
                break;
            }
            System.out.println(read(socket));
        }
    }

    private static String read(Socket socket) throws Exception {
        byte[] buffer = new byte[128];
        int len;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while (-1 != (len = socket.getInputStream().read(buffer, 0, buffer.length))) {
            bos.write(buffer, 0, len);
            if (buffer[len - 1] == '\n') {
                break;
            }
        }
        return new String(bos.toByteArray(), 0, bos.size());
    }
}
