package org.carl.sys.net.hashing.analysis;

/**
 * @author longjie
 * 2021/4/16
 */
public interface ArrayDataAnalysis {

    int getMinValue();

    int getMaxValue();

    int getMedian();

    void sortArray();
}
