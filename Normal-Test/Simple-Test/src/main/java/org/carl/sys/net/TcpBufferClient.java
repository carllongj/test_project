package org.carl.sys.net;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * @author longjie
 * 2021/1/26
 */
public class TcpBufferClient {
    public static void main(String[] args) throws Exception {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress("192.168.206.134", 4399));

        while (true) {
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            if (line.equals("status")) {
                System.out.println(String.format("r: %s,s: %s", socket.getReceiveBufferSize(),
                        socket.getSendBufferSize()));
            } else {
                Path path = Paths.get("F:\\test\\" + line + ".pdf");
                byte[] allBytes = Files.readAllBytes(path);
                socket.getOutputStream().write(allBytes);
                System.out.println("send finish");
            }
        }
    }
}
