/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.sys.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author carllongj
 * 2021/2/10 19:45
 */
public class LocalPortForwarding {

    private static Map<SelectionKey, SelectionKey> map = new HashMap<>();

    static final InetSocketAddress REMOTE_ADDRESS = new InetSocketAddress("192.168.206.134", 4396);

    public static void main(String[] args) throws IOException {
        ServerSocketChannel channel = ServerSocketChannel.open();
        channel.configureBlocking(false);
        channel.bind(new InetSocketAddress(37482));

        Selector selector = Selector.open();

        channel.register(selector, SelectionKey.OP_ACCEPT);

        while (selector.select() > 0) {
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();
                iterator.remove();

                if (selectionKey.isConnectable()) {

                    SocketChannel client = (SocketChannel) selectionKey.channel();
                    byte[] input = (byte[]) selectionKey.attachment();
                    if (client.finishConnect()) {
                        client.register(selector, SelectionKey.OP_WRITE);
                        selectionKey.attach(input);
                    } else {
                        System.out.println("Connection Failed");
                    }
                    continue;
                }

                if (selectionKey.isAcceptable()) {
                    ServerSocketChannel server = (ServerSocketChannel) selectionKey.channel();
                    SocketChannel client = server.accept();
                    client.configureBlocking(false);
                    client.register(selector, SelectionKey.OP_READ);
                    continue;
                }

                if (selectionKey.isReadable()) {
                    SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
                    if (socketChannel.getRemoteAddress().equals(REMOTE_ADDRESS)) {
                        //当前为真实响应
                        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                        int len;
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        while ((len = socketChannel.read(byteBuffer)) > 0) {
                            byteBuffer.flip();
                            bos.write(byteBuffer.array(), 0, len);
                            byteBuffer.clear();
                        }
                        SelectionKey clientKey = map.get(selectionKey);
                        SocketChannel client = (SocketChannel) clientKey.channel();
                        client.write(byteBuffer);
                    }

                    ByteBuffer buffer = ByteBuffer.allocate(1024);

                    int read = socketChannel.read(buffer);
                    buffer.flip();
                    SocketChannel forwarding = SocketChannel.open();
                    socketChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);

                    forwarding.configureBlocking(false);
                    boolean finished = forwarding.connect(REMOTE_ADDRESS);
                    SelectionKey key;
                    if (finished) {
                        key = forwarding.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                    } else {
                        key = forwarding.register(selector, SelectionKey.OP_CONNECT);
                    }
                    byte[] bytes = new byte[read];
                    buffer.get(bytes);
                    key.attach(bytes);

                    /**
                     * 建立映射关系
                     */
                    map.put(key, selectionKey);
                }

                if (selectionKey.isWritable()) {
                    SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
                    byte[] attachment = (byte[]) selectionKey.attachment();
                    if (null != attachment) {
                        ByteBuffer buffer = ByteBuffer.allocate(attachment.length);
                        buffer.put(attachment);
                        buffer.flip();
                        socketChannel.write(buffer);
                    }
                    socketChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                }
            }
        }
    }
}
