package org.carl.sys.net.hashing.node;

/**
 * @author longjie
 * 2021/4/9
 */
public class MachineNode {
    private String ip;

    private String name;

    public MachineNode(String ip, String name) {
        this.ip = ip;
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return ip;
    }
}
