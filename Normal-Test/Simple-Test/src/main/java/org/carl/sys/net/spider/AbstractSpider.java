package org.carl.sys.net.spider;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author longjie
 * 2021/4/29
 */
public abstract class AbstractSpider {

    private List<RequestHandler> requestHandlers = new ArrayList<>();

    public AbstractSpider addHandler(RequestHandler requestHandler) {
        this.requestHandlers.add(requestHandler);
        return this;
    }

    /**
     * 设置当前的请求头信息
     *
     * @return 返回请求头设置
     */
    protected abstract Map<String, String> getHeader();

    protected void customHttpClient(CloseableHttpClient httpClient) {
    }

    public final void catchData() {
        NetworkHandler handler = new NetworkHandler();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        customHttpClient(httpClient);
        handler.setHttpClient(httpClient);
        for (RequestHandler requestHandler : requestHandlers) {
            Map<String, String> map = getHeader();
            if (null != requestHandler.getHeaders() && !requestHandler.getHeaders().isEmpty()) {
                map.putAll(requestHandler.getHeaders());
            }
            String result = handler.doGet(requestHandler.getUrl(), map, null);
            requestHandler.getConsumer().accept(result);
        }
    }
}
