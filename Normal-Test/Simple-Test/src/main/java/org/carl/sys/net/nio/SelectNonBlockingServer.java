/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.sys.net.nio;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * @author carllongj
 * 2021/1/25 20:48
 */
public class SelectNonBlockingServer {
    public static void main(String[] args) throws Exception {

        //创建绑定服务端
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) ServerSocketChannel.open()
                .bind(new InetSocketAddress(4399)).configureBlocking(false);

        //执行选择器
        Selector selector = Selector.open();
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        tab:
        while (selector.select() > 0) {
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();

            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();
                if (selectionKey.isAcceptable()) {
                    ServerSocketChannel ssc = (ServerSocketChannel) selectionKey.channel();
                    SocketChannel socketChannel = ssc.accept();
                    socketChannel.configureBlocking(false);
                    socketChannel.register(selector, SelectionKey.OP_READ);
                } else if (selectionKey.isReadable()) {
                    SocketChannel channel = (SocketChannel) selectionKey.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(128);
                    int len = channel.read(buffer);
                    channel.read(buffer);
                    buffer.flip();
                    byte[] arr = new byte[len];
                    buffer.get(arr, 0, len);
                    channel.register(selector, SelectionKey.OP_WRITE, arr);
                } else if (selectionKey.isWritable()) {
                    SocketChannel channel = (SocketChannel) selectionKey.channel();
                    byte[] arr = (byte[]) selectionKey.attachment();
                    String str = new String(arr);
                    ByteBuffer buffer = ByteBuffer.allocate(128);
                    buffer.put(arr);
                    buffer.flip();
                    System.out.println(str);
                    channel.write(buffer);
                    if (str.endsWith("bye\n")) {
                        selectionKey.cancel();
                        channel.shutdownOutput();
                        channel.close();
                        break tab;
                    } else {
                        channel.register(selector, SelectionKey.OP_READ);
                    }
                }
                // 移除当前的KEY
                iterator.remove();
            }
        }
        selector.close();
        serverSocketChannel.close();
    }
}
