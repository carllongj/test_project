/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.sys.generic;

import java.util.ArrayList;
import java.util.List;

/**
 * @author carllongj
 *     <p>date 2022/7/29 11:12
 */
public class GenericClass {

  /**
   * 父接口的 声明为 泛型,则定义 的方法为 consume(Object obj)
   *
   * @param <T>
   */
  interface A<T> {
    void consume(T t);
  }

  /**
   * 子类实现了并定义了方法为 String,定义的方法为 consume(String str);
   *
   * <p>并不构成重新.是因为编译器在生成字节码时,构造了一个方法 consume(Object obj),
   *
   * <p>实际上调用的是构造的该方法,该方法与父类定义的方法才构成重写.
   *
   * <p>其实现也很简单,通过 强转将 obj转换成str,在通过调用子类中定义的 consume(String str) 来实现.
   */
  class B implements A<String> {

    @Override
    public void consume(String s) {
      System.out.println(s);
    }
  }

  public static void main(String[] args) {
    //
    List<String> list = new ArrayList<>();
    list.add("test");

    List<Integer> newList = new ArrayList<>();
    newList.add(102);
  }
}
