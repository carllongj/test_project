/*
 * Copyright 2021 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.sys.net.guava.hash;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.util.UUID;

/**
 * @author carllongj
 * 2021/4/9 20:33
 */
public class HashFunctionTest {

    public static void main(String[] args) {
        HashFunction function = Hashing.murmur3_128();
        HashCode hashCode = function.hashBytes(UUID.randomUUID().toString().getBytes());
        System.out.println(hashCode.asInt());
    }
}
