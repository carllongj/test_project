/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.sys.env;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author carllongj
 *     <p>date 2022/7/16 17:06
 */
public class UserDirMainApplication {

  @SuppressWarnings({"EmptyTryBlock", "ResultOfMethodCallIgnored"})
  public static void main(String[] args) throws FileNotFoundException {
    String property = System.getProperty("user.dir");
    String fileRelativePath = "generateByApplication/trash/userDirFile";

    Path path = Paths.get(property, fileRelativePath);
    File file = new File(property, fileRelativePath);

    // 两者的路径一定相同
    if (!path.toFile().getAbsolutePath().equals(file.getAbsolutePath())) {
      throw new RuntimeException(
          String.format(
              "path [%s] different from path [%s]",
              path.toFile().getAbsolutePath(), file.getAbsolutePath()));
    }

    // 删除文件
    if (file.exists() && file.isFile()) {
      file.delete();
    }
    try (FileOutputStream fos = new FileOutputStream(file)) {
      // just for generate an empty file
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    String trashPath = path.getParent().toFile().getAbsolutePath();
    System.setProperty("user.dir", trashPath);
    File userDirFile = new File("userDirFile");

    // 修改后的user.dir的userDirFile路径与 file的路径是一样的,但是这个路径通过文件系统访问却是不存在的.
    boolean flag =
        userDirFile.getAbsolutePath().equals(file.getAbsolutePath())
            && !userDirFile.exists()
            && !userDirFile.isFile()
            && !userDirFile.isDirectory()
            && !userDirFile.delete();

    if (!flag) {
      throw new RuntimeException("存在文件");
    }
  }
}
