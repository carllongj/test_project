package org.carl.sys.net.hashing.functions;

import java.util.Random;

/**
 * @author longjie
 * 2021/4/16
 */
public class IntegerHashFunction {

    /**
     * Thomas Wang's 32 bit Mix Function
     * 用以hash整数的函数
     *
     * @param key hash的key
     * @return 返回hash后的值
     */
    public int hashing(Integer key) {
        key += ~(key << 15);
        key ^= (key >> 10);
        key += (key << 3);
        key ^= (key >> 6);
        key += ~(key << 11);
        key ^= (key >> 16);
        return key;
    }

    public static void main(String[] args) {
        Random random = new Random();
        int[] table = new int[1024];
        for (int i = 0; i < 100000; i++) {
            int i1 = random.nextInt(1000000000);
            table[i1 % table.length] += 1;
        }

        for (int i = 0; i < table.length; i++) {
            System.out.println(String.format("当前索引位置 %d 的数据元素为 %d", i, table[i]));
        }
    }
}
