package org.carl.sys.net.spider;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author longjie
 * 2021/4/29
 */
public class NetworkHandler {

    protected HttpClient httpClient;

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public final String doRequest(HttpUriRequest request, Map<String, String> header, Consumer<HttpUriRequest> consumer) {
        addHeader(request, header);
        if (Objects.nonNull(consumer)) {
            consumer.accept(request);
        }
        try {
            return getResponse(request);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("获取数据失败");
        }
    }

    public String doGet(String url, Map<String, String> header, Consumer<HttpUriRequest> consumer) {
        return doRequest(new HttpGet(url), header, consumer);
    }

    public String doPost(String url, Map<String, String> header, Consumer<HttpUriRequest> consumer) {
        return doRequest(new HttpPost(url), header, consumer);
    }


    private void addHeader(HttpUriRequest request, Map<String, String> header) {
        if (Objects.nonNull(header) && !header.isEmpty()) {
            for (Map.Entry<String, String> entry : header.entrySet()) {
                if (Objects.nonNull(entry.getKey())) {
                    request.addHeader(entry.getKey(), entry.getValue());
                }
            }
        }
    }

    private String getResponse(HttpUriRequest request) throws IOException {
        HttpResponse httpResponse = httpClient.execute(request);
        return EntityUtils.toString(httpResponse.getEntity());
    }
}
