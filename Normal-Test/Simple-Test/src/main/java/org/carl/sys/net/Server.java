/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.sys.net;

import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author longjie
 * 2021/1/20
 */
public class Server {

    private static class Run implements Runnable {

        private Socket socket;

        Run(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                byte[] buffer = new byte[128];
                String hostAddress = socket.getInetAddress().getHostAddress();
                System.out.println("HOST IP: " + hostAddress);
                while (true) {
                    int read = socket.getInputStream().read(buffer, 0, buffer.length);
                    if (-1 != read) {
                        String info = new String(buffer, 0, read);
                        if ("bye\n".equalsIgnoreCase(info)) {
                            System.out.println("close client");
                            socket.getOutputStream().write("close\n".getBytes());
                            socket.shutdownOutput();
                            break;
                        } else if (info.startsWith("file:")) {
                            String substring = info.substring(5, info.length() - 1);
                            System.out.println("read a file,path is : " + substring);
                            byte[] allBytes = Files.readAllBytes(Paths.get(substring));
                            socket.getOutputStream().write(allBytes);
                        } else {
                            socket.getOutputStream().write((info + "\n").getBytes());
                        }
                    }
                }
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(4399);

        while (true) {
            Socket socket = serverSocket.accept();
            new Thread(new Run(socket)).start();
        }
    }
}
