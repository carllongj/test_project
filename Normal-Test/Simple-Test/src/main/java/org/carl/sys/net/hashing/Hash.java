package org.carl.sys.net.hashing;

/**
 * @author longjie
 * 2021/4/9
 */
public interface Hash {

    /**
     * 进行Hash运行
     *
     * @param key 指定的key
     * @return 返回对应的hash值
     */
    Long hash(String key);
}
