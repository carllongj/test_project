package org.carl.sys.net.spider;

import org.apache.http.HttpHeaders;

import java.util.HashMap;
import java.util.Map;

/**
 * @author longjie
 * 2021/4/29
 */
public class MtSpider extends AbstractSpider {

    @Override
    protected Map<String, String> getHeader() {
        Map<String, String> header = new HashMap<>(8);
        header.put(HttpHeaders.ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        header.put(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
        header.put(HttpHeaders.ACCEPT_LANGUAGE, "zh-CN,zh;q=0.9,en;q=0.8");
        header.put(HttpHeaders.CACHE_CONTROL, "no-cache");
        header.put(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");
        return header;
    }
}
