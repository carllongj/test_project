/*
 * Copyright 2020 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl;

/**
 * @author carllongj
 * 2020/8/8 14:59
 */
public class ProgressOutput {

    public static void main(String[] args) throws InterruptedException {
        int count = 0;
        while (true) {
            if (count == 100) {
                count = 0;
            }
            System.out.print(String.format("%3d%%", count));
            System.out.print("\t");
            for (int i = 0; i < count; i++) {
                if (count % 4 == 0) {
                    System.out.print("*");
                }
            }
            System.out.print("*");
            System.out.print("\r");
            count++;
            Thread.sleep(1000);
        }
    }
}
