package org.carl.math;

public class MathTest {

    public static void main(String[] args) {
        System.out.println(Math.floorDiv(0x100000 - 1, 10000));
        System.out.println(Math.ceil(1294.000000001));

        int total = 10001;
        int size = 10000;
        //当前数据的块位置
        double blockSize = total / size;
        //一个sheet支持的块数量
        double currentSheetBlockSize = Math.floorDiv(0x100000 + 1, size);
        //当前数据应该放入的sheet位置
        double sheetIndex = Math.ceil(blockSize / currentSheetBlockSize);
        System.out.println(sheetIndex);
        //生成sheet的名称
        String sheetName = "Sheet" + (int) sheetIndex;
        System.out.println(sheetName);
        blockSize = (total / size);
        currentSheetBlockSize = Math.floorDiv(0x100000 + 1, size);
        double v = Math.ceil(blockSize / currentSheetBlockSize);
//        double v = Math.ceil((total / size) / Math.floorDiv(0x100000 + 1, size));
        System.out.println(v);
        System.out.println("Sheet" + (int) v);
    }
}
