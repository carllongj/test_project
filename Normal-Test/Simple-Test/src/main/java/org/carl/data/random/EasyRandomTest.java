package org.carl.data.random;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;

import java.nio.charset.StandardCharsets;

public class EasyRandomTest {

  public static class A {
    private String name;

    private int age;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public int getAge() {
      return age;
    }

    public void setAge(int age) {
      this.age = age;
    }

    @Override
    public String toString() {
      return "A{" +
              "name='" + name + '\'' +
              ", age=" + age +
              '}';
    }
  }

  public static void main(String[] args) {
    //
    EasyRandomParameters parameters = new EasyRandomParameters();
    parameters.excludeField(FieldPredicates.named("age").and(FieldPredicates.ofType(int.class)))
            .charset(StandardCharsets.UTF_8);
    A object = new EasyRandom(parameters).nextObject(A.class);
    System.out.println(object);
  }
}
