package org.carl.data.structure.skiplist;

import java.security.SecureRandom;
import java.util.Objects;

public class SkipList<T extends Comparable<T>, V> {

    private SkipListEntry<T, V> head;

    private static final SecureRandom random = new SecureRandom();

    /**
     * 获取列表的下一个元素
     *
     * @param k 指定的KEY
     * @return 返回对应的数据
     */
    public V get(T k) {
        Objects.requireNonNull(k);
        SkipListEntry<T, V> skipListEntry = this.findClosestEntry(k);
        if (Objects.nonNull(skipListEntry) &&
                skipListEntry.getKey().compareTo(k) == 0) {
            return skipListEntry.getValue();
        }
        return null;
    }

    private SkipListEntry<T, V> findClosestEntry(T key) {
        if (Objects.isNull(head)) {
            return null;
        }

        SkipListEntry<T, V> pre = head;
        // 开始查询一个元素值大于该值的
        while (Objects.nonNull(pre.getNext()) &&
                pre.getNext().getKey().compareTo(key) < 0) {
            pre = pre.getNext();
        }

        // 查询下一层级
        while (Objects.nonNull(pre.getNextLevel()) && pre.getNextLevel() != null) {
            pre = pre.getNextLevel();
            while (Objects.nonNull(pre.getNext()) &&
                    pre.getNext().getKey().compareTo(key) < 0) {
                pre = pre.getNext();
            }
        }

        // 表示后一个元素值是大于该值的
        while (Objects.nonNull(pre.getNext()) &&
                pre.getNext().getKey().compareTo(key) <= 0) {
            pre = pre.getNext();
        }

        return pre;
    }

    /**
     * 写入元素
     *
     * @param key   指定的KEY
     * @param value 写入的Value
     */
    public V put(T key, V value) {
        SkipListEntry<T, V> skipListEntry = this.findClosestEntry(key);
        // 元素存在,则进行替换即可
        if (Objects.nonNull(skipListEntry) &&
                skipListEntry.getKey().compareTo(key) == 0) {
            V oldValue = skipListEntry.getValue();
            skipListEntry.setValue(value);
            return oldValue;
        }
        // 不存在则需要插入数据
        SkipListEntry<T, V> entry = new SkipListEntry<>();
        entry.setKey(key);
        entry.setValue(value);
        // 只有head为null时,才会导致查询的entry为null.
        if (Objects.isNull(skipListEntry)) {
            head = entry;
            return null;
        }

        SkipListEntry<T, V> next = skipListEntry.getNext();
        skipListEntry.setNext(entry);
        entry.setNext(next);
        return null;
    }
}