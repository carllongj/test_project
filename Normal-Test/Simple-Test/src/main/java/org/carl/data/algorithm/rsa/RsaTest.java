/*
 * Copyright 2021 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.data.algorithm.rsa;


import javax.crypto.Cipher;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * @author carllongj
 * 2021/1/16 11:18
 */
public class RsaTest {

    public static void main(String[] args) throws Exception {
        /*String encode = encode("hello,World");
        String decode = decode(encode);*/

        String encode = encodeByPrivateKey("Hello,World");
        String decode = decodeByPrivateKey(encode);
    }

    private static String encode(String text) throws Exception {
        byte[] bytes = Files.readAllBytes(Paths.get("./id_rsa.pub"));
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(
                new X509EncodedKeySpec(Base64.getDecoder().decode(bytes)));
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        String encodeToString = Base64.getEncoder().encodeToString(cipher.doFinal(text.getBytes()));
        System.out.println(encodeToString);
        return encodeToString;
    }

    private static String decode(String encrypt) throws Exception {
        byte[] bytes = Files.readAllBytes(Paths.get("./id_rsa"));
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(
                new PKCS8EncodedKeySpec(Base64.getDecoder().decode(bytes)));
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decode = Base64.getDecoder().decode(encrypt);
        String result = new String(cipher.doFinal(decode));
        System.out.println(result);
        return result;
    }

    private static String encodeByPrivateKey(String text) throws Exception {
        byte[] bytes = Files.readAllBytes(Paths.get("./id_rsa"));
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(
                new PKCS8EncodedKeySpec(Base64.getDecoder().decode(bytes)));

        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        String encodeToString = Base64.getEncoder().encodeToString(cipher.doFinal(text.getBytes()));
        System.out.println(encodeToString);
        return encodeToString;
    }

    private static String decodeByPrivateKey(String encrypt) throws Exception {
        byte[] bytes = Files.readAllBytes(Paths.get("./id_rsa.pub"));
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(
                new X509EncodedKeySpec(Base64.getDecoder().decode(bytes)));
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] decode = Base64.getDecoder().decode(encrypt);
        String result = new String(cipher.doFinal(decode));
        System.out.println(result);
        return result;
    }

    private static void generateKey() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048, new SecureRandom());
        KeyPair keyPair = generator.generateKeyPair();

        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        Files.write(Paths.get("./id_rsa"), Base64.getEncoder().encode(privateKey.getEncoded()), StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
        Files.write(Paths.get("./id_rsa.pub"), Base64.getEncoder().encode(publicKey.getEncoded()), StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
    }
}
