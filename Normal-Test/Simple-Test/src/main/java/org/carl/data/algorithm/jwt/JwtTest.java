package org.carl.data.algorithm.jwt;

import io.jsonwebtoken.*;

import java.util.Base64;
import java.util.Date;

public class JwtTest {

  public static void main(String[] args) {
    //
    String compact =
        Jwts.builder()
            .setHeaderParam("typ", "jwt")
            .setHeaderParam("alg", "HS256")
            .claim("userId", "1")
            .claim("username", "org/carl")
            .claim("role", "123456")
            //            .setSubject("JWT-TEST")
            .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60))
            //            .setId(UUID.randomUUID().toString())
            .signWith(SignatureAlgorithm.HS256, "123456")
            .compact();

    System.out.println(compact);

    JwtParser parser = Jwts.parser();
    Jwt jwt = parser.setSigningKey("123456").parse(compact);

    Object body = jwt.getBody();
    System.out.println(body);
    Header header = jwt.getHeader();
    System.out.println(header);

    System.out.println(
        " payload -> "
            + new String(
                Base64.getDecoder()
                    .decode(
                        "eyJ1c2VySWQiOiIxIiwidXNlcm5hbWUiOiJjYXJsIiwicm9sZSI6IjEyMzQ1NiIsImV4cCI6MTY1NjA2MjY4MH0")));
    System.out.println(
        " value -> "
            + new String(Base64.getDecoder().decode("eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9")));
  }
}
