package org.carl.data.structure.skiplist;

public class SkipListEntry<T extends Comparable<T>, V> {

    /**
     * 列表中的有序排序KEY
     */
    private T key;

    /**
     * 存储的数据节点
     */
    private V value;

    /**
     * 同级别
     */
    private SkipListEntry<T, V> next;

    /**
     * 下一个层级的元素
     */
    private SkipListEntry<T, V> nextLevel;

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public SkipListEntry<T, V> getNext() {
        return next;
    }

    public void setNext(SkipListEntry<T, V> next) {
        this.next = next;
    }

    public SkipListEntry<T, V> getNextLevel() {
        return nextLevel;
    }

    public void setNextLevel(SkipListEntry<T, V> nextLevel) {
        this.nextLevel = nextLevel;
    }
}
