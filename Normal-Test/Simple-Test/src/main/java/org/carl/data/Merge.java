/*
 * Copyright 2020 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.data;

import org.apache.commons.io.FileExistsException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author carllongj
 * 2020/7/7 20:44
 */
public class Merge {
    public static final String inputPath = "D:\\work\\learn\\data\\lol\\area\\23\\info\\";

    public static final String outputPath = "D:\\work\\learn\\data\\lol\\area\\23\\single";

    public static void main(String[] args) throws IOException {
        File file = new File(inputPath);
        File[] files = file.listFiles();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYYMMdd");
        File destFile = new File(outputPath + File.separator + formatter.format(LocalDate.now()));
        if (destFile.exists()) {
            throw new FileExistsException(String.format("File %s already exists", destFile.getAbsolutePath()));
        }
        FileOutputStream outputStream = new FileOutputStream(destFile);
        for (File file1 : files) {
            byte[] bytes = Files.readAllBytes(file1.toPath());
            outputStream.write((file1.getName() + "\t").getBytes(StandardCharsets.UTF_8));
            outputStream.write(bytes);
            outputStream.write('\n');
        }

        outputStream.flush();
        outputStream.close();
    }
}
