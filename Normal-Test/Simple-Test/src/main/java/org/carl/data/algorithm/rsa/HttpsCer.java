/*
 * Copyright 2021 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.data.algorithm.rsa;

import sun.misc.BASE64Encoder;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.PrivateKey;

/**
 * @author carllongj
 * 2021/4/22 20:16
 */
public class HttpsCer {
    public static void main(String[] args) {
        try {
            BASE64Encoder encoder = new BASE64Encoder();
            //读取文件内容
            FileInputStream is = new FileInputStream("src/main/resources/keystore.p12");
            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(is, "123456".toCharArray());
            PrivateKey key = (PrivateKey) ks.getKey("carl", "123456".toCharArray());
            String encoded = encoder.encode(key.getEncoded());
            System.out.println(encoded);
            is.close();
        } catch (Exception e) {
        }
    }
}
