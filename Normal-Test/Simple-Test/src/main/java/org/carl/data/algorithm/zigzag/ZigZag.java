/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.data.algorithm.zigzag;

/**
 * @author carllongj
 *     <p>date 2022/7/31 11:07
 */
public class ZigZag {

  public static void main(String[] args) {
    //
    /*
    System.out.println(int32ToZigZag(0));		// 0
    System.out.println(int32ToZigZag(-1));		// 1
    System.out.println(int32ToZigZag(1));		// 2
    System.out.println(int32ToZigZag(-2));		// 3
    System.out.println(long32ToZigZag(2147483647));		// 4294967294
    System.out.println(long32ToZigZag(-2147483648));		// 4294967295
    */

    int value = -5;
    System.out.println(Integer.toBinaryString(value << 1));
    System.out.println(Integer.toBinaryString(value >> 31));

    System.out.println(zigzagToInt32(1));
    System.out.println(zigzagToInt32(2));
    System.out.println(zigzagToInt32(3));
    System.out.println(zigzagToInt32(0x12));
  }

  /**
   * 使用 zigzag 对数值进行编码
   *
   * @param value 指定的 value
   * @return 返回编码后的内容
   */
  public static int int32ToZigZag(int value) {
    /*
      value << 1 表示去掉符号位的有效位,且末位添加了0.
      value >> 31 表示得到一个全0(正数为全0)或者全1(负数为全1)的序列
    */
    return (value << 1) ^ (value >> 31);
  }

  /**
   * 解码 zigzag 编码
   *
   * @param value 指定的编码后数据
   * @return 返回解码后真实的数据
   */
  public static int zigzagToInt32(int value) {
    /*
     还原 zigzag 映射
     (value & 1) 得到末位,判断序列应该是0还是1来进行还原原本的数据.
     	* 若当前值为0,表示之前的符号位为0 -> 因为之前的末位是0,异或的规则为相同为0,不同为1,序列号全为0.
     	* 若当前值为1,表示之前符号位为1 -> 因为之前的末位是0,说明当前的符号位必须要序列全为1.

     	(value >> 1) 则表示去掉了符号位,得到了有效的位.再做一次异或操作将还原成为原来的值.
    */
    return (value >> 1) ^ -(value & 1);
  }

  public static long long32ToZigZag(long value) {
    return (value << 1) ^ (value >> 63);
  }
}
