package org.carl.learn.skiplist;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.carl.data.structure.skiplist.SkipList;
import org.carl.data.structure.skiplist.SkipListEntry;

import java.lang.reflect.Field;

public class SkipListTest {

    Field field;

    @BeforeTest
    public void init() throws NoSuchFieldException {
        field = SkipList.class.getDeclaredField("head");
        field.setAccessible(true);

    }

    private void valueOf(Integer value, SkipListEntry<Integer, String> target,
                         SkipListEntry<Integer, String> next, SkipListEntry<Integer, String> nextLevel) {
        target.setValue(value.toString());
        target.setKey(value);
        target.setNext(next);
        target.setNextLevel(nextLevel);
    }

    @Test
    public void testGet() throws IllegalAccessException {
        SkipList<Integer, String> skipList = new SkipList<>();
        SkipListEntry<Integer, String> head = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry5_level1 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry1_level2 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry3_level2 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry5_level2 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry7_level2 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry1_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry2_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry3_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry4_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry5_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry6_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry7_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry8_level3 = new SkipListEntry<>();
        SkipListEntry<Integer, String> entry9_level3 = new SkipListEntry<>();
        valueOf(1, head, entry5_level1, entry1_level2);
        valueOf(5, entry5_level1, null, entry5_level2);
        valueOf(1, entry1_level2, entry3_level2, entry1_level3);
        valueOf(3, entry3_level2, entry5_level2, entry3_level3);
        valueOf(5, entry5_level2, entry7_level2, entry5_level3);
        valueOf(7, entry7_level2, null, entry7_level3);
        valueOf(1, entry1_level3, entry2_level3, null);
        valueOf(2, entry2_level3, entry3_level3, null);
        valueOf(3, entry3_level3, entry4_level3, null);
        valueOf(4, entry4_level3, entry5_level3, null);
        valueOf(5, entry5_level3, entry6_level3, null);
        valueOf(6, entry6_level3, entry7_level3, null);
        valueOf(7, entry7_level3, entry8_level3, null);
        valueOf(8, entry8_level3, entry9_level3, null);
        valueOf(9, entry9_level3, null, null);
        field.set(skipList, head);

        Assert.assertEquals("7", skipList.get(7));
    }
}
