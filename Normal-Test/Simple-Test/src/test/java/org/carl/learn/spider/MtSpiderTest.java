package org.carl.learn.spider;

import org.carl.sys.net.spider.MtSpider;
import org.carl.sys.net.spider.RequestHandler;
import com.google.gson.Gson;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author longjie
 * 2021/4/29
 */
public class MtSpiderTest {

    private static final String PROVINCE_CITYS = "https://www.meituan.com/changecity/";

    private static final String GET_PROVINCE_CITY_INFO = "https://www.meituan.com/ptapi/getprovincecityinfo/";

    static Pattern pattern = Pattern.compile("<a href=\".*?\" class=\"link city ?\">.+?</a>");

    private MtSpider mtSpider;

    private Gson gson = new Gson();

    @BeforeTest
    public void init() {
        mtSpider = new MtSpider();
    }

    @Test
    public void testSpider() {
        MtSpider spider = new MtSpider();
        spider
                .addHandler(new RequestHandler(PROVINCE_CITYS, str -> {
                    Matcher matcher = pattern.matcher(str);
                    while (matcher.find()) {
                        System.out.println(matcher.group());
                    }
                }))

                .addHandler(new RequestHandler(GET_PROVINCE_CITY_INFO, str -> {

                }));
        spider.catchData();


    }
}
