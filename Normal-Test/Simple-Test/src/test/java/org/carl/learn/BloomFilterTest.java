package org.carl.learn;

import com.google.common.hash.Funnels;

import java.nio.charset.StandardCharsets;

public class BloomFilterTest {
  public static void main(String[] args) {
    int total = 10000000;
    com.google.common.hash.BloomFilter<CharSequence> filter =
        com.google.common.hash.BloomFilter.create(
            Funnels.stringFunnel(StandardCharsets.UTF_8), total, 0.00003);

    for (int i = 0; i < total; i++) {
      filter.put("" + i);
    }

    int count = 0;

    for (int i = 0; i < total + 10000; i++) {
      if (filter.mightContain("" + i)) {
        count++;
      }
    }

    System.out.println(count);
  }
}
