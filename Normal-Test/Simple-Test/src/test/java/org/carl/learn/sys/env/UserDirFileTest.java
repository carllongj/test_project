/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.learn.sys.env;

import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author carllongj
 *     <p>date 2022/7/16 17:25
 */
public class UserDirFileTest {

  @Test
  public void test() {
    System.out.println(System.getProperty("user.dir"));
  }

  @Test
  public void testFile() throws IOException {
    File file = new File(System.getProperty("user.dir"), "./../../");
    System.out.println(file.getAbsolutePath());
    /*
     * getCanonicalPath方法可以正确的解析 ../ 或者 ./ 路径
     * 而 getAbsolutePath 则不可以正确解析
     */
    System.out.println(file.getCanonicalPath());
  }

  @Test
  public void testLocalFileUrl() {
    String path = "file://D:\\soft\\install";
    try {
      URL url = new URL(path);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }
}
