/*
 * Copyright 2020 carllongj
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.carl.learn;

import java.util.Scanner;

/**
 * @author carllongj
 * 2020/8/8 15:43
 */
public class LearnTest {

    public static void testSwitch(int score) {
        /*
            1. 90及 分以上,输出优秀
            2. 80-89 输出良好
            3. 70-79 输出普通
            4. 60 - 69 输出一般
            5. 60以下 输出不及格
         */

        //输入一个成绩
        /*if (score >= 90) {
            System.out.println("优秀");
        } else if (score >= 80) {
            System.out.println("良好");

        } else if (score >= 70) {
            System.out.println("普通");

        } else if (score >= 60) {
            System.out.println("一般");
        } else {
            System.out.println("不及格");
        }*/
        int month;
        switch (score) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                month = 31;
                break;
            case 2:
                month = 28;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                month = 30;
                break;
            default:
                throw new IllegalArgumentException();
        }
        System.out.println(score + "月份有" + month + "天");
    }

    public static void main(String[] args) {
        System.out.println("请输入成绩: ");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int score = scanner.nextInt();
            testSwitch(score);
            System.out.println("请输入成绩: ");
        }
    }
}
