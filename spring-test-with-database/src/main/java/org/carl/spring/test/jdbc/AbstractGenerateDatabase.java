/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.test.jdbc;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.sql.Connection;
import java.util.*;

/**
 * @author carllongj
 *     <p>date 2022/7/16 9:25
 */
public abstract class AbstractGenerateDatabase {

  private static final String SCRIPTS_SQL_BASE_PATH = "/scripts/sql/";

  protected Connection connection;

  public List<File> getAllSqlScripts() {
    URL resource = this.getClass().getResource(SCRIPTS_SQL_BASE_PATH);
    if (Objects.isNull(resource)) {
      return Collections.emptyList();
    }
    File file = new File(resource.getFile());
    if (file.exists() && file.isDirectory()) {
      List<File> listFile = new ArrayList<>();
      File[] allScripts = file.listFiles();
      if (Objects.isNull(allScripts)) {
        return Collections.emptyList();
      }
      for (File script : allScripts) {
        if (script.exists() && script.canRead()) {
          listFile.add(script);
        }
      }
      listFile.sort(Comparator.comparing(File::getName));
      return listFile;
    }
    return Collections.emptyList();
  }

  /** 删除原来的脏数据库,并且生成新的数据库 */
  public final void generateDatabase() {
    this.destroyDatabase();
    this.connection = this.createConnection();
    for (File file : getAllSqlScripts()) {
      if (file.isFile() && file.canRead()) {
        try (FileReader reader = new FileReader(file)) {
          // 执行脚本
          new ScriptRunner(this.connection).runScript(reader);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

  /**
   * 创建的数据库连接
   *
   * @return 返回数据库连接
   */
  protected abstract Connection createConnection();

  /** 删除当前的数据库 */
  protected abstract void destroyDatabase();
}
