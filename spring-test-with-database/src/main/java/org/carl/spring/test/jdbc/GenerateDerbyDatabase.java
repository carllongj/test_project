/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.test.jdbc;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author carllongj
 *     <p>date 2022/7/16 9:29
 */
public class GenerateDerbyDatabase extends AbstractGenerateDatabase {

  /** 创建数据库的名称 */
  private static final String DB_NAME = "testdb";

  /** 数据库创建路径 */
  private static final String DB_PATH = "generateByApplication/db/derby/" + DB_NAME;

  /** 连接串 */
  public static final String DB_CONNECTION_STR = "jdbc:derby:" + DB_PATH;

  /** 驱动类 */
  private static final String DRIVER_CLASS = "org.apache.derby.jdbc.EmbeddedDriver";

  static {
    // 设置 derby.log 生成位置
    System.setProperty("derby.stream.error.file", "generateByApplication/trash/derby.log");
  }

  @Override
  protected Connection createConnection() {
    String createPath = DB_CONNECTION_STR + ";create=true";
    try {
      // 注册驱动
      DriverManager.registerDriver((Driver) Class.forName(DRIVER_CLASS).newInstance());
      return DriverManager.getConnection(createPath);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void destroyDatabase() {
    // 首先关闭数据库
    try {
      DriverManager.registerDriver((Driver) Class.forName(DRIVER_CLASS).newInstance());
      DriverManager.getConnection("jdbc:derby:;shutdown=true");
    } catch (SQLException e) {
      // ignore
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    // 删除数据库文件
    this.deleteDatabaseFile();
  }

  private void deleteDatabaseFile() {
    File file = new File(DB_PATH);
    if (file.exists() && file.isDirectory()) {
      // 删除文件,若删除的是目录,则需要将底层文件全部删除完成后才能删除目录.
      try {
        FileUtils.deleteDirectory(file);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  public static void main(String[] args) {
    GenerateDerbyDatabase derbyDatabase = new GenerateDerbyDatabase();
    derbyDatabase.generateDatabase();
  }
}
