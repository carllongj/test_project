/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.test;

import org.carl.spring.test.bean.ContextBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.sql.DataSource;

/**
 * @author carllongj
 *     <p>date 2022/7/15 23:58
 */
@ContextConfiguration(locations = {"classpath:/applicationContext.xml"})
public class SpringTest extends AbstractTestNGSpringContextTests {

  @Autowired private ContextBean contextBean;

  @Autowired private DataSource dataSource;

  @BeforeClass
  public void setUp() {
    System.setProperty("derby.stream.error.file", "generateByApplication/trash/derby.log");
    System.out.println(System.getProperty("user.dir"));
  }

  @Test
  public void testSpringTest() {
    System.out.println(contextBean);
  }
}
