/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author carllongj date 2022/7/16 8:37
 */
@ContextConfiguration({"classpath:/applicationContext.xml"})
public class SpringTestWithTx extends AbstractTransactionalTestNGSpringContextTests {

  @BeforeClass
  public void setUp() {
    System.setProperty("derby.stream.error.file", "../generateByApplication/trash/derby.log");
  }

  @Test
  public void testQuery() {
    // 初始化情况下,数据库记录数为5
    this.countEquals(5);
  }

  @Test
  public void testDeleteWithTx() {
    String sql = "delete from test_user where id = 5";
    this.jdbcTemplate.execute(sql);
    this.countEquals(4);
  }

  @SuppressWarnings("divzero")
  @Test
  public void testDeleteError() {
    String sql = "delete from test_user";
    this.jdbcTemplate.execute(sql);
  }

  private void countEquals(int count) {
    String sql = "select count(*) from test_user";
    assertThat(this.jdbcTemplate.queryForObject(sql, Long.class)).isEqualTo(count);
  }

  @AfterClass
  public void AfterRollback() {
    this.countEquals(5);
  }
}
