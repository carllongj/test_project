/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.test.database.jdbc;

import org.carl.spring.test.jdbc.GenerateDerbyDatabase;
import org.testng.annotations.Test;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author carllongj
 *     <p>date 2022/7/16 11:17
 */
public class GenerateDerbyDatabaseTest {

  @Test
  public void testGetAllScripts() {
    GenerateDerbyDatabase derbyDatabase = new GenerateDerbyDatabase();
    List<File> allSqlScripts = derbyDatabase.getAllSqlScripts();
    assertThat(allSqlScripts).hasSize(1);
  }

  @Test
  public void testGenerateTable() throws SQLException {
    GenerateDerbyDatabase derbyDatabase = new GenerateDerbyDatabase();
    derbyDatabase.generateDatabase();

    Connection connection = DriverManager.getConnection(GenerateDerbyDatabase.DB_CONNECTION_STR);
    ResultSet resultSet =
        connection.createStatement().executeQuery("select count(*) from test_user");
    while (resultSet.next()) {
      assertThat(resultSet.getInt(1)).isEqualTo(1);
    }
  }

  @Test
  public void testDestroyDatabase() throws SQLException {
    GenerateDerbyDatabase derbyDatabase = new GenerateDerbyDatabase();
    derbyDatabase.destroyDatabase();
  }
}
