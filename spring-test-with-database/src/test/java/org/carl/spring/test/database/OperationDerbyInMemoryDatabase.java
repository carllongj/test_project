/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.spring.test.database;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.springframework.core.io.ClassPathResource;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author carllongj
 *     <p>date 2022/7/16 20:21
 */
@SuppressWarnings("NewClassNamingConvention")
public class OperationDerbyInMemoryDatabase {

  @BeforeClass
  public void setUp() throws Exception {
    System.setProperty("derby.stream.error.file", "generateByApplication/trash/derby.log");
    // Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
    DriverManager.registerDriver(
        (Driver) Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance());
  }

  @Test
  public void createMemoryDatabase() throws SQLException, IOException {
    Connection connection =
        DriverManager.getConnection("jdbc:derby:memory:testdb;create=true", "sa", "");
    assertThat(connection).isNotNull();
    new ScriptRunner(connection)
        .runScript(
            new InputStreamReader(
                new ClassPathResource("scripts/sql/test.sql").getInputStream()));
    ResultSet rs = connection.createStatement().executeQuery("select count(*) from test_user");
    while (rs.next()) {
      assertThat(rs.getInt(1)).isEqualTo(1);
    }
  }
}
