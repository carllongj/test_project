## 模块说明
* 该模块用于测试部分数据库与spring相关.

### 数据库
* 数据库使用的 apache的 derby 数据
* 数据库的路径在 项目根路径下的 `generateByApplication/db/derby` 数据库路径下.
* 该目录由于不会保存数据,并且可以任意删除.数据库`相关SQL`将需要以文件的形式写入到`resources/scripts/sql`下
  * 该目录下的文件必须要日SQL脚本的创建日期来进行命名,其内部会以时间先后顺序来将排序进行创建.
  * SQL 脚本中尽量不存在依赖关系,若确实有依赖,则被依赖的日期需要在依赖之前.
* 通过 GenerateDerbyDataBase 类来进行恢复Derby数据库的数据.
