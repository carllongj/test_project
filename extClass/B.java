public class B {

    static {
        System.out.println("static block");
    }

    public static void fun() throws Exception {
        Class clazz = Class.forName("com.carl.test.classloader.PrintTestClass");
        System.out.println(Thread.currentThread().getContextClassLoader());
        System.out.println(ClassLoader.getSystemClassLoader());
        System.out.println(clazz.getClassLoader().getClass().getName());
        clazz.getDeclaredMethod("fun", (Class<?>[]) null).invoke(null, (Object[]) null);
    }
}