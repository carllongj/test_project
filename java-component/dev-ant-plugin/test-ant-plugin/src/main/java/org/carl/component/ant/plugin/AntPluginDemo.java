package org.carl.component.ant.plugin;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class AntPluginDemo extends Task {

  private String name;

  private String doSome;

  @Override
  public void execute() throws BuildException {
    String rst = "Hello world ！" + name + " , " + doSome;
    getProject().setUserProperty(name, rst);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDoSome() {
    return doSome;
  }

  public void setDoSome(String doSome) {
    this.doSome = doSome;
  }
}
