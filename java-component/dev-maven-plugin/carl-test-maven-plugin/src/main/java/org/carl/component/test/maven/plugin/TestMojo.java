package org.carl.component.test.maven.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "goalOne")
public class TestMojo extends AbstractMojo {

    @Parameter(name = "key")
    private String key;

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info(String.format("key = %s", key));
        getLog().info(String.format("location at (%s,%s,%s),buildDir = %s,buildDir = %s", project.getGroupId(), project.getArtifactId(), project.getVersion(), project.getBasedir(), project.getBuild().getOutputDirectory()));
    }
}
