package org.carl.component.test.maven.plugin.dependecies;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;

import java.util.List;

@Mojo(name = "printDependencies")
public class DependenciesPrinter extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${settings}")
    private Settings settings;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        getLog().info("settings repository : " + settings.getLocalRepository());
        List dependencies = project.getDependencies();
        getLog().info("dependencies list : " + dependencies);
        List plugins = project.getBuildPlugins();
        getLog().info("buildPlugins list : " + plugins);
    }
}
