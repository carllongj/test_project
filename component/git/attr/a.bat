@rem
@rem@rem Copyright 2015 the original author or authors.
@rem
@rem@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem @rem You may obtain a copy of the License at
@rem @rem
@rem @rem      https://www.apache.org/licenses/LICENSE-2.0
@rem @rem
@rem @rem Unless required by applicable law or agreed to in writing, software
@rem @rem distributed under the License is distributed on an "AS IS" BASIS,
@rem @rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem @rem See the License for the specific language governing permissions and
@rem @rem limitations under the License.
@rem @rem
