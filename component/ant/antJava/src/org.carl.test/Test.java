package org.carl.test;

public class Test {

    public static void main(String[] args) {
        System.out.println(java.util.Arrays.toString(args));
        System.out.println("System Property = " + System.getProperty("pyt") + " -> " + System.getProperty("maven.home"));
        System.out.println("Environment Property = " + System.getenv("pyt"));
    }
}