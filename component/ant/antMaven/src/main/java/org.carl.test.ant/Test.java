package org.carl.test.ant;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Test {
    public static void main(String[] args) throws Exception {
        InputStream asStream = Test.class.getResourceAsStream("ant.properties");
        Properties prop = new Properties();
        prop.load(asStream);
    }
}