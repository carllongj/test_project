## kafka 下载安装
* 配置均在 wsl 环境下运行
* 将解压后的安装路径加入到 PATH 中
### 安装和配置
* `下载kafka,解压和进入到目录`
  * `wget https://www.apache.org/dyn/closer.cgi?path=/kafka/2.4.1/kafka_2.12-2.4.1.tgz`
  * `tar kafka_2.12-2.4.1.tgz && cd kafka_2.12-2.4.1`
* 单机版启动`kafka服务`
  * 首先启动`zookeeper`.
    * 修改 `config/zookeeper.properties` 配置文件
      * 配置 `zookeeper` 的 `dataDir` 属性的路径地址.
      * 配置 `zookeeper` 的 `dataLogDir` 属性的路径地址.
    * 启动`zookeeper`.
      * `zookeeper-server-start.sh -daemon config/zookeeper.properties`
  * 启动kafka服务
    * 修改 `config/server.properties` 配置文件
      * 配置 `broker.id`,每一个 `broker.id` 必须是不重复的整数,用以标识一个`broker`.
      * 配置 `listeners` 配置项, 该项用以配置当前 `broker` 的监听地址.
    * 启动 `kafka`
      * `kafka-server-start.sh -daemon config/server.properties`  
* wsl 目录切换到 component/kafka 级别,执行上述命令均可正常执行.
* 
