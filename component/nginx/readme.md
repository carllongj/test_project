# nginx 安装与部署
* nginx在linux中的安装(`源码安装`)
    1. 下载并且解压redis的源码包.
        * `wget http://nginx.org/download/nginx-1.19.6.tar.gz && tar -zxf nginx-1.19.6.tar.gz && cd nginx-1.19.6`
    2. 进入目录下配置 `configure`.
        * `./configure --prefix=/opt/soft/dev/nginx --with-http_stub_status_module --with-http_ssl_module --with-pcre --with-stream`
            * `--prefix=<path>` 用来指定安装的路径地址.
            * `--with-http_stub_status_module` 启用 `server status` 页(可有可无).
            * `--with-http_ssl_module` 开启HTTP SSL模块,使NGINX可以支持HTTPS请求.需要安装了OPENSSL.
            * `--with-pcre` 用以支持`HTTP Rewrite`模块.
            * `--with-mail` 启用 `IMAP4/POP3/SMTP` 代理模块.
            * `--with-stream` 开启`四层负载均衡和代理`(让协议不单单支持http,`基于传输层的协议都可以进行代理`).
    3. 执行安装
        *  `make && make install`
* 安装测试目录结构
    * nginx -- 整个nginx 安装目录结构
         * project.conf 通用配置,定义了http一些默认配置
    * nginx 启动命令需要在对应的nginx.conf目录下,通过 `-p . -c nginx.conf -e ../../.log/error.log` 来启动
