/*
 * MIT License
 *
 * Copyright (c) 2019-2022 carllongj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.carl.learn.spring.boot.mock;

import org.carl.spring.boot.web.bean.TestUser;
import org.carl.spring.boot.web.dao.TestUserMapper;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author carllongj
 *     <p>date 2022/7/16 19:50
 */
public class MockTest {

  @Test
  public void testMock() {
    TestUserMapper userMapper = mock(TestUserMapper.class);
    TestUser testUser = userMapper.selectOne(1L);
    assertThat(testUser).isNull();

    TestUserMapper mockUserMapper = mock(TestUserMapper.class);

    when(mockUserMapper.selectOne(1L)).thenReturn(new TestUser());
    when(mockUserMapper.selectOne(2L)).thenReturn(null);
    assertThat(mockUserMapper.selectOne(1L)).isNotNull();
    assertThat(mockUserMapper.selectOne(2L)).isNull();
  }

  @Test
  public void testDoReturn() {
    List<String> list = new LinkedList<>();
    List<String> spyList = spy(list);

    // when(spyList.get(0)).thenReturn("test");
    String foo = doReturn("foo").when(spyList).get(0);
  }
}
