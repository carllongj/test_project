package org.carl.spring.boot.cron;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DefaultPropertyCronSelector implements CronSelector {

    @Value("${cron.expression}")
    private String cron;

    @Override
    public String getCron() {
        return this.cron;
    }
}
