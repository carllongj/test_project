package org.carl.spring.boot.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 第二种方式创建定时任务
 */
//@Configuration
//@EnableScheduling
public class DynamicScheduleTask implements SchedulingConfigurer {

    @Autowired
    private CronSelector cronSelector;

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(() -> {
            try {
                Thread.sleep(8000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("currentTime: " + dateTimeFormatter.format(LocalDateTime.now()));
        }, triggerContext -> new CronTrigger(cronSelector.getCron()).nextExecutionTime(triggerContext));
    }
}
