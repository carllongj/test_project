package org.carl.spring.boot.cron;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/*
@Component
@EnableAsync
*/
public class AsyncScheduleTask {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    @Async
    @Scheduled(cron = "${cron.expression}")
    public void scheduleTask() throws InterruptedException {
        Thread.sleep(8000);
        System.out.println("currentTime: " + dateTimeFormatter.format(LocalDateTime.now()));
    }
}
