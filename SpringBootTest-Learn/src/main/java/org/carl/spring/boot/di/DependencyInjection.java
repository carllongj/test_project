package org.carl.spring.boot.di;

import org.springframework.beans.factory.annotation.Value;

public class DependencyInjection {

  @Value("${test.prop.value}")
  private String value;

  public String getValue() {
    return value;
  }
}
