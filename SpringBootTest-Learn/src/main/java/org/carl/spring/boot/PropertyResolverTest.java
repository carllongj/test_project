package org.carl.spring.boot;

import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySourcesPropertyResolver;

import java.util.HashMap;
import java.util.Map;

public class PropertyResolverTest {

	public static void main(String[] args) {
		Map<String, Object> map = new HashMap<>();
		map.put("jdk", "1.8");
		map.put("linux.default.jdk", "abs.${jdk}");
		map.put("apache", "maven");

		MapPropertySource propertySource = new MapPropertySource("TEST", map);
		MutablePropertySources propertySources = new MutablePropertySources();
		propertySources.addFirst(propertySource);
		PropertySourcesPropertyResolver resolver = new PropertySourcesPropertyResolver(propertySources);
		String placeholders = resolver.resolvePlaceholders("fks = ${linux.default.jdk}");
		System.out.println(placeholders);
	}
}
