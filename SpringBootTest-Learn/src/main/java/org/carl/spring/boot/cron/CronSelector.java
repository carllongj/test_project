package org.carl.spring.boot.cron;

public interface CronSelector {

    /**
     * 获取cron表达式
     *
     * @return 返回cron表达式
     */
    String getCron();
}
