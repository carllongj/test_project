/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot.config;

import org.springframework.context.annotation.Configuration;

/**
 * @author carllongj
 * 2022/7/7 23:23
 */
@Configuration
public class ConfigurationBeanAnnotation {

    //@Bean(name = "configBean1")
    public ConfigurationPropertiesBean createBean() {
        return new ConfigurationPropertiesBean();
    }
}
