/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot.metadata;

import org.carl.spring.boot.condition.config.ConditionalConfig;
import org.carl.spring.boot.controller.TestController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.type.StandardAnnotationMetadata;
import org.springframework.stereotype.Controller;

/**
 * @author carllongj
 * 2022/7/8 23:17
 */
public class MetadataReader {
    public static void main(String[] args) {
        StandardAnnotationMetadata metadata = new StandardAnnotationMetadata(ConditionalConfig.class);
        boolean annotated = metadata.isAnnotated(Conditional.class.getName());
        System.out.println(annotated);
        boolean methods = metadata.hasAnnotatedMethods(Bean.class.getName());
        System.out.println(methods);

        metadata = new StandardAnnotationMetadata(TestController.class);
        annotated = metadata.isAnnotated(Controller.class.getName());
        System.out.println(annotated);

    }
}
