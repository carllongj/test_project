package org.carl.spring.boot.nacos;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigChangeEvent;
import com.alibaba.nacos.api.config.ConfigChangeItem;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.client.config.listener.impl.AbstractConfigChangeListener;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

//@Component
//@ConfigurationProperties(prefix = "nacos.monitor")
public class NacosUpdateListener implements InitializingBean {

  private static final String NACOS_MONITOR_PREFIX = "nacos.monitor";

  @Value("${nacos.config.server-addr}")
  private String serverAddr;

  @Value("${nacos.config.namespace}")
  private String namespace;

  /** 外部配置监控 */
  @NestedConfigurationProperty private List<MonitorConfig> config;

  public List<MonitorConfig> getConfig() {
    return config;
  }

  public void setConfig(List<MonitorConfig> config) {
    this.config = config;
  }

  /** 服务监控 */
  private ConfigService configService;

  public static class MonitorConfig {
    private String dataId;

    private String group;

    public String getDataId() {
      return dataId;
    }

    public void setDataId(String dataId) {
      this.dataId = dataId;
    }

    public String getGroup() {
      return group;
    }

    public void setGroup(String group) {
      this.group = group;
    }
  }

  public static class DataSourceMonitor {

    private String id;

    private String database;

    private String driver;

    private String url;

    private String username;

    private String password;

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getDatabase() {
      return database;
    }

    public void setDatabase(String database) {
      this.database = database;
    }

    public String getDriver() {
      return driver;
    }

    public void setDriver(String driver) {
      this.driver = driver;
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    if (CollectionUtils.isEmpty(config)) {
      return;
    }
    Properties properties = new Properties();
    properties.put(PropertyKeyConst.SERVER_ADDR, serverAddr);
    properties.put(PropertyKeyConst.NAMESPACE, namespace);
    configService = NacosFactory.createConfigService(properties);
    for (MonitorConfig config : config) {
      configService.addListener(
          config.getDataId(),
          config.getGroup(),
          new AbstractConfigChangeListener() {

            private ThreadLocal<String> threadLocal = new ThreadLocal<>();
            Map<String, Map<String, String>> cache;

            @Override
            public void receiveConfigChange(ConfigChangeEvent event) {
              /*
               1. 遍历所有的变化的条目
               2. 通过条目对应的KEY,找到对应的配置项
               3. 更新对应的数据库内容
              */
              Map<String, Map<String, String>> cache = parseYamlToMap(threadLocal.get());
              //
              Map<String, Map<String, String>> update = new HashMap<>();
              for (ConfigChangeItem changeItem : event.getChangeItems()) {
                String key = changeItem.getKey();
                update.putIfAbsent(getPrefixKey(key), new HashMap<>());
                update.get(getPrefixKey(key)).put(getSpiltKey(key), changeItem.getNewValue());
              }
              // 数据变更处理完毕
              if (CollectionUtils.isEmpty(update)) {
                return;
              }
              // 处理所有修改的内容
              for (Map.Entry<String, Map<String, String>> entry : update.entrySet()) {
                Map<String, String> value = entry.getValue();
                String id = value.get("id");
                if (Objects.isNull(id)) {
                  continue;
                }
              }
            }

            private Map<String, Map<String, String>> parseYamlToMap(String configInfo) {
              // 拿到整个配置的内容
              YamlPropertiesFactoryBean factoryBean = new YamlPropertiesFactoryBean();
              factoryBean.setResources(
                  new ByteArrayResource(configInfo.getBytes(StandardCharsets.UTF_8)));
              factoryBean.afterPropertiesSet();
              //
              Properties properties = factoryBean.getObject();
              Map<String, Map<String, String>> cache = new HashMap<>();
              for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                String key = (String) entry.getKey();
                String prefixKey = getPrefixKey(key);
                if (!cache.containsKey(prefixKey)) {
                  cache.put(prefixKey, new HashMap<>());
                }
                cache.get(prefixKey).put(getSpiltKey(key), (String) entry.getValue());
              }
              return cache;
            }

            @Override
            public void receiveConfigInfo(String configInfo) {
              threadLocal.set(configInfo);
            }

            public Long getId(String idStr) {
              return Long.parseLong(idStr.substring(0, idStr.indexOf('-')));
            }

            public String getPrefixKey(String str) {
              return str.substring(0, str.lastIndexOf('.'));
            }

            public String getSpiltKey(String str) {
              return str.substring(str.lastIndexOf('.') + 1);
            }
          });
    }
  }
}
