/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot.config;

/**
 * @author carllongj
 * 2022/7/7 23:47
 */
public class Config {

    private String name = "carllongj";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Config{" +
                "name='" + name + '\'' +
                '}';
    }
}
