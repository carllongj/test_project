package org.carl.spring.boot;

import org.carl.spring.boot.condition.config.ConditionalConfig;
import org.carl.spring.boot.config.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAutoConfiguration
@EnableScheduling
@ComponentScan("org.carl.spring.boot")
public class StartSpringBootApplication {

    public static void main(String[] args) {
        System.setProperty("os.name", "linux");
        // 启动 SpringBoot程序
        SpringApplication application = new SpringApplication(StartSpringBootApplication.class);
        ConfigurableApplicationContext context = application.run();
        System.out.println(context.getBean(Config.class));
        System.out.println(context.getBean(ConditionalConfig.class));
        System.out.println(context.getBean("linux"));
        System.out.println(context.getBean("windows"));
    }
}
