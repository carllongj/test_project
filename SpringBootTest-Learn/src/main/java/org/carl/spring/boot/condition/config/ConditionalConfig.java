/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot.condition.config;

import org.carl.spring.boot.RepeatCondition;
import org.carl.spring.boot.condition.ConditionalOnLinux;
import org.carl.spring.boot.condition.ConditionalOnWindows;
import org.carl.spring.boot.condition.bean.LinuxConfigBean;
import org.carl.spring.boot.condition.bean.WindowsConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author carllongj
 * 2022/7/8 23:00
 */
@Configuration
//@Conditional(NotImportCondition.class)
@RepeatCondition
public class ConditionalConfig {

    @Bean("windows")
    @ConditionalOnWindows
    public WindowsConfigBean createWindowsConfigBean() {
        return new WindowsConfigBean();
    }

    @Bean("linux")
    @ConditionalOnLinux
    public LinuxConfigBean createLinuxConfigBean() {
        return new LinuxConfigBean();
    }
}
