/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot.config;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

/**
 * @author carllongj
 * 2022/7/7 23:46
 */
@Import(Config.class)
@Component
public class SimpleImport {
}
