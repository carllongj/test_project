package org.carl.spring.boot.cron;


import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 单线程创建定时任务,超时会影响第二次任务执行
 */
@Configuration
public class CronCreator {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private AtomicInteger testCount = new AtomicInteger(0);

    //@Scheduled(cron = "0/5 * * * * ?")
    public void configTask() throws InterruptedException {
        Thread.sleep(8000);
        int andIncrement = testCount.getAndIncrement();
        System.out.println("currentTime: " + dateTimeFormatter.format(LocalDateTime.now()));
    }

    @Scheduled(cron = "${cron.expression}")
    public void getCronTask() {
        System.out.println("Class : " + this.getClass().getName() + "currentTime: " + dateTimeFormatter.format(LocalDateTime.now()));
    }
}
