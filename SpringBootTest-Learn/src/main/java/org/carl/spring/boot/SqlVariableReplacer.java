package org.carl.spring.boot;

import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySources;
import org.springframework.core.env.PropertySourcesPropertyResolver;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class SqlVariableReplacer {

  /** 符合的key */
  private static final Set<String> VALID_KEY = new HashSet<>();

  static {
    VALID_KEY.add("userId");
    VALID_KEY.add("orgId");
    VALID_KEY.add("username");
  }

  private SqlVariableBean sqlVariableBean;

  /** 待解析的SQL */
  private final String sql;

  private String prefix;

  private String suffix;

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  public SqlVariableBean getSqlVariableBean() {
    return sqlVariableBean;
  }

  public void setSqlVariableBean(SqlVariableBean sqlVariableBean) {
    this.sqlVariableBean = sqlVariableBean;
  }

  public SqlVariableReplacer(String sql) {
    Objects.requireNonNull(sql);
    this.sql = sql;
  }

  /**
   * 是否符合
   *
   * @param key 指定的KEY
   * @return 返回是否合法
   */
  public boolean isValidKey(String key) {
    return VALID_KEY.contains(key);
  }

  /**
   * 解析替换SQL中的固定变量
   *
   * @return 返回替换后的变量
   */
  public String replace() {
    if (Objects.isNull(sqlVariableBean)) {
      return this.sql;
    }
    PropertySourcesPropertyResolver propertyResolver =
        new PropertySourcesPropertyResolver(this.sqlVariableBean.parsePropertySource());
    if (StringUtils.hasText(this.prefix)) {
      propertyResolver.setPlaceholderPrefix(this.prefix);
    }
    if (Objects.nonNull(this.suffix)) {
      propertyResolver.setPlaceholderSuffix(this.suffix);
    }
    return propertyResolver.resolveRequiredPlaceholders(this.sql);
  }

  /** 外部传入对应的变量信息 */
  public static class SqlVariableBean {

    /** 访问者信息 */
    private String visitor;

    /** 用户名 */
    private String username;

    /** 组织机构ID */
    private String orgId;

    public String getVisitor() {
      return visitor;
    }

    public void setVisitor(String visitor) {
      this.visitor = visitor;
    }

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public String getOrgId() {
      return orgId;
    }

    public void setOrgId(String orgId) {
      this.orgId = orgId;
    }

    public PropertySources parsePropertySource() {
      MutablePropertySources propertySources = new MutablePropertySources();
      Map<String, Object> prop = new HashMap<>();
      prop.put("userId", visitor);
      prop.put("orgId", this.orgId);
      prop.put("username", this.username);
      propertySources.addFirst(new MapPropertySource("REPLACE", prop));
      return propertySources;
    }
  }

  public static void main(String[] args) {
    SqlVariableReplacer replacer =
        new SqlVariableReplacer(
            "select * from test where user_id = ${userId} and org_id = ${orgId} and username=${username}");
    SqlVariableReplacer replacer1 =
        new SqlVariableReplacer(
            "select * from test where user_id = :userId and org_id = :orgId and username=:username ");
    replacer1.setPrefix(":");
    replacer1.setSuffix(" ");
    SqlVariableBean sqlVariableBean = new SqlVariableBean();
    sqlVariableBean.setVisitor("2345");
    sqlVariableBean.setOrgId("1234");
    sqlVariableBean.setUsername("carllongj");
    replacer.setSqlVariableBean(sqlVariableBean);
    replacer1.setSqlVariableBean(sqlVariableBean);
    String replace = replacer.replace();
    System.out.println(replace);
    String replace1 = replacer1.replace();
    System.out.println(replace1);
  }
}
