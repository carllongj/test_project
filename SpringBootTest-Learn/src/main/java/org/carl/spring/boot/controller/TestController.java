package org.carl.spring.boot.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class TestController implements ApplicationContextAware {

  @NacosValue(value = "${test.var.key:nullValue}", autoRefreshed = true)
  private String value;

  @NacosValue(value = "${local.serv.port:nullValue}", autoRefreshed = true)
  private String port;

  @Value(value = "${test.prop.value:nullValue}")
  private String nacosValue;

  @GetMapping("/get")
  @ApiOperation("获取当前的cookie的内容信息")
  public String get(HttpServletRequest request) {
    Cookie[] requestCookies = request.getCookies();

    StringBuilder sb = new StringBuilder("Cookie: \n");
    if (java.util.Objects.nonNull(requestCookies) && requestCookies.length > 0) {
      for (Cookie cookie : requestCookies) {
        sb.append(
            String.format("%s -> %s;%s\n", cookie.getName(), cookie.getValue(), cookie.getPath()));
      }
    }

    return "{\"get\": \"" + value + "\",\"cookie\":" + sb + "  }";
  }

  @GetMapping("/getPort")
  public String test() {
    return "{\"get\": \"" + port + "\"}";
  }

  @GetMapping("/nacosValue")
  public String nacosValue() {
    return "{\"get\": \"" + nacosValue + "\"}";
  }

  @GetMapping("/redirect")
  public void redirect(HttpServletRequest request, HttpServletResponse response) throws Exception {
    String url = request.getParameter("url");
    response.setStatus(302);
    response.sendRedirect(url);
  }

  @GetMapping("/cookie")
  public void setCookie(HttpServletRequest request, HttpServletResponse response) throws Exception {
    Cookie cookie = new Cookie("CookieKey", "CookieValue");
    cookie.setPath("/");
    response.addCookie(cookie);
  }

  @GetMapping("/cookieCon")
  public void setCookiePath(HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    Cookie cookie = new Cookie("CookieKey", "CookieValue");
    cookie.setPath(request.getContextPath());
    response.addCookie(cookie);
  }

  private ApplicationContext context;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.context = applicationContext;
  }
}
