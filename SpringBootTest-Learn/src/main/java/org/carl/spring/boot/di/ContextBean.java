package org.carl.spring.boot.di;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ContextBean implements ApplicationContextAware, InitializingBean {

  private ApplicationContext applicationContext;

  @Override
  public void afterPropertiesSet() throws Exception {
    DependencyInjection di = new DependencyInjection();
    applicationContext.getAutowireCapableBeanFactory().autowireBean(di);
    System.out.println(di.getValue());
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
