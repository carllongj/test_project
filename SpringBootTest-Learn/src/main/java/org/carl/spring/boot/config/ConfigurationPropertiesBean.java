/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author carllongj
 * 2022/7/7 23:21
 */
@ConfigurationProperties(value = "user.info")
@Component("configBean2")
public class ConfigurationPropertiesBean {

    private String name;

    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "ConfigurationPropertiesBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
