/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot.swagger;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;

/**
 * @author carllongj
 * 2022/7/6 21:41
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
public class SwaggerConfig implements ApplicationListener<EmbeddedServletContainerInitializedEvent> {

    @Bean
    public Docket buildDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                // 设置api的说明信息
                .apiInfo(apiInfo())
                // 添加全局参数
                .globalOperationParameters(Collections.emptyList())
                // 添加选择器
                .select()
                // 选择controller包下的api
                .apis(RequestHandlerSelectors.basePackage("org.carl.spring.boot.controller"))
                // 所有路径
                .paths(PathSelectors.any())
                .build();
    }

    public ApiInfo apiInfo() {
        Contact contact = new Contact("carl", "https://carllongj.github.io", "carllongj@gmail.com");
        return new ApiInfoBuilder()
                .contact(contact)
                .title("Carl's SpringBoot Test")
                .description("Swagger Test")
                .version("1.0.0")
                .build();
    }


    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent embeddedServletContainerInitializedEvent) {
        try {
            String address = InetAddress.getLocalHost().getHostAddress();
            String name = embeddedServletContainerInitializedEvent.getApplicationContext().getApplicationName();
            String swagger = String.format("http://%s:%d%s/swagger-ui.html", address, embeddedServletContainerInitializedEvent.getSource().getPort(), name);
            String knife4j = String.format("http://%s:%d%s/doc.html", address, embeddedServletContainerInitializedEvent.getSource().getPort(), name);
            System.out.println("swagger-ui 地址为 : " + swagger);
            System.out.println("knife4j 地址为 : " + knife4j);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
