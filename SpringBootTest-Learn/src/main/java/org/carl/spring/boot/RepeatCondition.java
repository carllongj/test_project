/*
 * Copyright 2022 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.spring.boot;

import org.carl.spring.boot.condition.NoCondition;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author carllongj
 * 2022/7/9 0:26
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@NoCondition
public @interface RepeatCondition {
}
