package org.carl.spring.boot.nacos;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;

import java.util.Properties;

public class NacosMain {

  public static void main(String[] args) throws Exception {
    //
    String serverAddr = "192.168.206.134:8848";
    // Data Id
    String dataId = "template.properties";
    // Group
    String group = "test";
    // Namespace
    String namespace = "5833d555-4bdb-41cd-bcc2-80dbdb49df3b";

    Properties properties = new Properties();
    properties.put("serverAddr", serverAddr);
    properties.put("namespace", namespace);
    // 获取配置
    ConfigService configService = NacosFactory.createConfigService(properties);
    String config = configService.getConfig(dataId, group, 5000);
    System.out.println(config);
  }
}
