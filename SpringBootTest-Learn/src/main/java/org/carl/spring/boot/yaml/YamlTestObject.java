package org.carl.spring.boot.yaml;

import org.yaml.snakeyaml.Yaml;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YamlTestObject {

  private static Object object;

  static {
    Map map = new HashMap();
    Map datasource = new HashMap();
    List config = new ArrayList();
    map.put("datasource", datasource);
    datasource.put("config", config);
    for (int i = 1; i < 4; i++) {
      HashMap hashMap = new HashMap();
      hashMap.put("key-" + i, "value-" + i);
      hashMap.put("key-" + (i + 4), "value0" + (i + 4));
      config.add(hashMap);
    }
    object = map;
  }

  public static void main(String[] args) {
    Yaml yaml = new Yaml();
    StringWriter writer = new StringWriter();
    yaml.dump(object, writer);
    System.out.println(writer);
  }
}
